cd /home/newthechainbeta/public_html
rm -rf var/page_cache 
rm -rf var/composer_home
rm -rf var/generation
rm -rf var/di
rm -rf var/view_preprocessed

php bin/magento setup:upgrade
php bin/magento indexer:reindex
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento setup:static-content:deploy en_GB
rm -fr /home/newthechainbeta/lscache/*
php bin/magento cache:flush

##/opt/cpanel/ea-php70/root/usr/bin/php bin/magento setup:upgrade
##/opt/cpanel/ea-php70/root/usr/bin/php bin/magento indexer:reindex
##/opt/cpanel/ea-php70/root/usr/bin/php bin/magento setup:di:compile
##/opt/cpanel/ea-php70/root/usr/bin/php bin/magento setup:static-content:deploy
##/opt/cpanel/ea-php70/root/usr/bin/php bin/magento setup:static-content:deploy en_GB
##rm -fr /home/thechain/lscache/*
##/opt/cpanel/ea-php70/root/usr/bin/php bin/magento cache:flush
