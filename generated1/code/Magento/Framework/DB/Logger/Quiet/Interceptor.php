<?php
namespace Magento\Framework\DB\Logger\Quiet;

/**
 * Interceptor class for @see \Magento\Framework\DB\Logger\Quiet
 */
class Interceptor extends \Magento\Framework\DB\Logger\Quiet implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct()
    {
        $this->___init();
    }

    /**
     * {@inheritdoc}
     */
    public function log($str)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'log');
        if (!$pluginInfo) {
            return parent::log($str);
        } else {
            return $this->___callPlugins('log', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function logStats($type, $sql, $bind = array(), $result = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'logStats');
        if (!$pluginInfo) {
            return parent::logStats($type, $sql, $bind, $result);
        } else {
            return $this->___callPlugins('logStats', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function critical(\Exception $e)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'critical');
        if (!$pluginInfo) {
            return parent::critical($e);
        } else {
            return $this->___callPlugins('critical', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function startTimer()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'startTimer');
        if (!$pluginInfo) {
            return parent::startTimer();
        } else {
            return $this->___callPlugins('startTimer', func_get_args(), $pluginInfo);
        }
    }
}
