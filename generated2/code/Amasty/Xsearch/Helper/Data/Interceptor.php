<?php
namespace Amasty\Xsearch\Helper\Data;

/**
 * Interceptor class for @see \Amasty\Xsearch\Helper\Data
 */
class Interceptor extends \Amasty\Xsearch\Helper\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Model\Config $configAttribute, \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection $collection, \Magento\Search\Helper\Data $searchHelper, \Magento\Framework\App\Helper\Context $context)
    {
        $this->___init();
        parent::__construct($configAttribute, $collection, $searchHelper, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleConfig($path)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getModuleConfig');
        if (!$pluginInfo) {
            return parent::getModuleConfig($path);
        } else {
            return $this->___callPlugins('getModuleConfig', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function highlight($text, $query)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'highlight');
        if (!$pluginInfo) {
            return parent::highlight($text, $query);
        } else {
            return $this->___callPlugins('highlight', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBlocksHtml(\Magento\Framework\View\Layout $layout)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBlocksHtml');
        if (!$pluginInfo) {
            return parent::getBlocksHtml($layout);
        } else {
            return $this->___callPlugins('getBlocksHtml', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductAttributes($requiredData = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductAttributes');
        if (!$pluginInfo) {
            return parent::getProductAttributes($requiredData);
        } else {
            return $this->___callPlugins('getProductAttributes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSingleProductRedirect()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isSingleProductRedirect');
        if (!$pluginInfo) {
            return parent::isSingleProductRedirect();
        } else {
            return $this->___callPlugins('isSingleProductRedirect', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResultUrl($query = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResultUrl');
        if (!$pluginInfo) {
            return parent::getResultUrl($query);
        } else {
            return $this->___callPlugins('getResultUrl', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isNoIndexNoFollowEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isNoIndexNoFollowEnabled');
        if (!$pluginInfo) {
            return parent::isNoIndexNoFollowEnabled();
        } else {
            return $this->___callPlugins('isNoIndexNoFollowEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSeoUrlsEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isSeoUrlsEnabled');
        if (!$pluginInfo) {
            return parent::isSeoUrlsEnabled();
        } else {
            return $this->___callPlugins('isSeoUrlsEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSeoKey()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSeoKey');
        if (!$pluginInfo) {
            return parent::getSeoKey();
        } else {
            return $this->___callPlugins('getSeoKey', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
