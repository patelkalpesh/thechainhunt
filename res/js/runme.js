jQuery(document).ready(function () {
  jQuery('#prod_cat_slider').slick({
    // centerMode: true,
    infinite: false,
    variableWidth: true,
    draggable: true,
    // slidesToShow : 11,
    // slidesToScroll: 1,
    // centerPadding: '20px',
    // edgeFriction: '1.00',
    prevArrow: '<button class="slick-prev"><i class="material-icons">chevron_left</i></button>',
    nextArrow: '<button class="slick-next"><i class="material-icons">chevron_right</i></button>',
    swipe: true,
    swipeToSlide: true,
    initialSlide: 0,
    // slidesToScroll: 2,
    slidesPerRow: 11,

    responsive: [
      // {
      //   breakpoint: 1024,
      //   settings: {
      //     slidesToShow: 3,
      //     slidesToScroll: 3,
      //     infinite: true,
      //     dots: true
      //   }
      // },
      // {
      //   breakpoint: 600,
      //   settings: {
      //     slidesToShow: 2,
      //     slidesToScroll: 2
      //   }
      // },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  jQuery("#prod_cat_slider").css({
    "display": "flex",
    "align-items": "center"
  });

  jQuery('#blog_slider').slick({
    // centerMode: true,
    infinite: false,
    variableWidth: true,
    draggable: true,
    // slidesToShow : 11,
    // slidesToScroll: 1,
    // centerPadding: '20px',
    // edgeFriction: '1.00',
    prevArrow: '<button class="slick-prev"><i class="material-icons">chevron_left</i></button>',
    nextArrow: '<button class="slick-next"><i class="material-icons">chevron_right</i></button>',
    swipe: true,
    swipeToSlide: true,
    initialSlide: 0,
    // slidesToScroll: 4,

    responsive: [
      // {
      //   breakpoint: 1024,
      //   settings: {
      //     slidesToShow: 3,
      //     slidesToScroll: 3,
      //     infinite: true,
      //     dots: true
      //   }
      // },
      // {
      //   breakpoint: 600,
      //   settings: {
      //     slidesToShow: 2,
      //     slidesToScroll: 2
      //   }
      // },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  jQuery("#blog_slider").css({
    "display": "flex",
    "align-items": "center"
  });

  jQuery("#aw-filter-price").click( function() {
    jQuery('.price-filter-info').contents().filter((_, el) => el.nodeType === 3).remove();
  });


});

//jQuery('.prod_cat_slider').slick('slickNext');