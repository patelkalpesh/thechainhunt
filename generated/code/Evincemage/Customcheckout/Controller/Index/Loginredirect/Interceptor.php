<?php
namespace Evincemage\Customcheckout\Controller\Index\Loginredirect;

/**
 * Interceptor class for @see \Evincemage\Customcheckout\Controller\Index\Loginredirect
 */
class Interceptor extends \Evincemage\Customcheckout\Controller\Index\Loginredirect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Registry $registry, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Session\SessionManagerInterface $coreSession, \Magento\Framework\App\Request\Http $request)
    {
        $this->___init();
        parent::__construct($context, $registry, $storeManager, $coreSession, $request);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
