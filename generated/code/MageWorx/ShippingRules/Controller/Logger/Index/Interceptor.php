<?php
namespace MageWorx\ShippingRules\Controller\Logger\Index;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Logger\Index
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Logger\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \MageWorx\ShippingRules\Model\Logger $logger)
    {
        $this->___init();
        parent::__construct($context, $logger);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
