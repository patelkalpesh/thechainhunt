<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Rate\Save;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Rate\Save
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Rate\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\App\Response\Http\FileFactory $fileFactory, \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter, \MageWorx\ShippingRules\Api\RateRepositoryInterface $rateRepository, \Magento\Directory\Model\RegionFactory $regionFactory, \Magento\Framework\DataObjectFactory $dataObjectFactory, \Psr\Log\LoggerInterface $logger, \Magento\Directory\Api\CountryInformationAcquirerInterface $countryInformationAcquirer)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $fileFactory, $dateFilter, $rateRepository, $regionFactory, $dataObjectFactory, $logger, $countryInformationAcquirer);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
