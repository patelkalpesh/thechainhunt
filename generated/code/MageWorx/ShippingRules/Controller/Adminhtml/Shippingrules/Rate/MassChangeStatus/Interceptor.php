<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Rate\MassChangeStatus;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Rate\MassChangeStatus
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Rate\MassChangeStatus implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, $collectionFactory = null, $entityFactory = null, $aclResourceName = null, $activeFieldName = null, $activeRequestParamName = null)
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory, $entityFactory, $aclResourceName, $activeFieldName, $activeRequestParamName);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
