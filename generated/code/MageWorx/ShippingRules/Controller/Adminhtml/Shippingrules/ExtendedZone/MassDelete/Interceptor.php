<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\MassDelete;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\MassDelete
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\MassDelete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \MageWorx\ShippingRules\Model\ResourceModel\ExtendedZone\CollectionFactory $collectionFactory, $aclResourceName = 'MageWorx_ShippingRules::extended_zones')
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory, $aclResourceName);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
