<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\Save;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\Save
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\DataObject\Factory $dataObjectFactory, \MageWorx\ShippingRules\Api\ExtendedZoneRepositoryInterface $zoneRepository, \Psr\Log\LoggerInterface $logger)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $dataObjectFactory, $zoneRepository, $logger);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
