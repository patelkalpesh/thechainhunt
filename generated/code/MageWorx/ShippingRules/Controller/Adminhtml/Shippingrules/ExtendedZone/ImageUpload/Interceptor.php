<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\ImageUpload;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\ImageUpload
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\ExtendedZone\ImageUpload implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \MageWorx\ShippingRules\Api\ExtendedZoneRepositoryInterface $zoneRepository, \Psr\Log\LoggerInterface $logger, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \MageWorx\ShippingRules\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $zoneRepository, $logger, $storeManager, $resultRawFactory, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
