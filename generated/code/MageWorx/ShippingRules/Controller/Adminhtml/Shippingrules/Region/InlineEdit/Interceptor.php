<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Region\InlineEdit;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Region\InlineEdit
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Region\InlineEdit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \MageWorx\ShippingRules\Api\RegionRepositoryInterface $regionRepository, \Magento\Framework\Controller\Result\JsonFactory $jsonFactory)
    {
        $this->___init();
        parent::__construct($context, $regionRepository, $jsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
