<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Region\Edit;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Region\Edit
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\Shippingrules\Region\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \MageWorx\ShippingRules\Api\RegionRepositoryInterface $regionRepository, \Psr\Log\LoggerInterface $logger)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $regionRepository, $logger);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
