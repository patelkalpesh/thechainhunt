<?php
namespace MageWorx\ShippingRules\Controller\Adminhtml\ImportExport\Index;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Adminhtml\ImportExport\Index
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Adminhtml\ImportExport\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Response\Http\FileFactory $fileFactory, \MageWorx\ShippingRules\Api\ExportHandlerInterfaceFactory $exportHandlerFactory, \MageWorx\ShippingRules\Api\ImportHandlerInterfaceFactory $importHandlerFactory)
    {
        $this->___init();
        parent::__construct($context, $fileFactory, $exportHandlerFactory, $importHandlerFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
