<?php
namespace MageWorx\ShippingRules\Controller\Zone\Change;

/**
 * Interceptor class for @see \MageWorx\ShippingRules\Controller\Zone\Change
 */
class Interceptor extends \MageWorx\ShippingRules\Controller\Zone\Change implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory, \Magento\Framework\View\LayoutFactory $layoutFactory, \MageWorx\GeoIP\Model\Geoip $geoIp, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\DataObjectFactory $dataObjectFactory, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Directory\Model\CountryInformationAcquirer $countryInformationAcquirer)
    {
        $this->___init();
        parent::__construct($context, $resultForwardFactory, $layoutFactory, $geoIp, $customerSession, $dataObjectFactory, $resultJsonFactory, $checkoutSession, $countryInformationAcquirer);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
