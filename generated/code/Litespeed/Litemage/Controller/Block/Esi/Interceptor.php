<?php
namespace Litespeed\Litemage\Controller\Block\Esi;

/**
 * Interceptor class for @see \Litespeed\Litemage\Controller\Block\Esi
 */
class Interceptor extends \Litespeed\Litemage\Controller\Block\Esi implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Translate\InlineInterface $translateInline, \Litespeed\Litemage\Model\CacheControl $litemageCache)
    {
        $this->___init();
        parent::__construct($context, $translateInline, $litemageCache);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
