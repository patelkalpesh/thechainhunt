<?php
namespace Litespeed\Litemage\Controller\Shell\Purge;

/**
 * Interceptor class for @see \Litespeed\Litemage\Controller\Shell\Purge
 */
class Interceptor extends \Litespeed\Litemage\Controller\Shell\Purge implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\HTTP\Header $httpHeader, \Litespeed\Litemage\Model\CacheControl $litemageCache)
    {
        $this->___init();
        parent::__construct($context, $httpHeader, $litemageCache);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
