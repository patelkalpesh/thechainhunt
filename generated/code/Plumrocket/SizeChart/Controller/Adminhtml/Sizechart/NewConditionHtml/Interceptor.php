<?php
namespace Plumrocket\SizeChart\Controller\Adminhtml\Sizechart\NewConditionHtml;

/**
 * Interceptor class for @see \Plumrocket\SizeChart\Controller\Adminhtml\Sizechart\NewConditionHtml
 */
class Interceptor extends \Plumrocket\SizeChart\Controller\Adminhtml\Sizechart\NewConditionHtml implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Plumrocket\SizeChart\Model\SizechartFactory $sizechartFactory)
    {
        $this->___init();
        parent::__construct($context, $sizechartFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
