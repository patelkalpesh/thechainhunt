<?php
namespace Magento\Shipping\Model\Config;

/**
 * Interceptor class for @see \Magento\Shipping\Model\Config
 */
class Interceptor extends \Magento\Shipping\Model\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Shipping\Model\CarrierFactory $carrierFactory, array $data = array())
    {
        $this->___init();
        parent::__construct($scopeConfig, $carrierFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getActiveCarriers($store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActiveCarriers');
        if (!$pluginInfo) {
            return parent::getActiveCarriers($store);
        } else {
            return $this->___callPlugins('getActiveCarriers', func_get_args(), $pluginInfo);
        }
    }
}
