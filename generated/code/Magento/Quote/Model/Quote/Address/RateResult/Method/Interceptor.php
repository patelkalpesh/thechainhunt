<?php
namespace Magento\Quote\Model\Quote\Address\RateResult\Method;

/**
 * Interceptor class for @see \Magento\Quote\Model\Quote\Address\RateResult\Method
 */
class Interceptor extends \Magento\Quote\Model\Quote\Address\RateResult\Method implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, array $data = array())
    {
        $this->___init();
        parent::__construct($priceCurrency, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getData($key = '', $index = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getData');
        if (!$pluginInfo) {
            return parent::getData($key, $index);
        } else {
            return $this->___callPlugins('getData', func_get_args(), $pluginInfo);
        }
    }
}
