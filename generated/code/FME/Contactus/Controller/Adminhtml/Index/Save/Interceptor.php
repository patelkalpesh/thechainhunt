<?php
namespace FME\Contactus\Controller\Adminhtml\Index\Save;

/**
 * Interceptor class for @see \FME\Contactus\Controller\Adminhtml\Index\Save
 */
class Interceptor extends \FME\Contactus\Controller\Adminhtml\Index\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Magento\Framework\Escaper $escaper, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory, \FME\Contactus\Helper\Data $myModuleHelper)
    {
        $this->___init();
        parent::__construct($context, $dataPersistor, $transportBuilder, $escaper, $inlineTranslation, $scopeConfig, $dateFactory, $myModuleHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
