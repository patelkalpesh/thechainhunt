<?php
namespace FME\Contactus\Controller\Front\Save;

/**
 * Interceptor class for @see \FME\Contactus\Controller\Front\Save
 */
class Interceptor extends \FME\Contactus\Controller\Front\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory, \FME\Contactus\Helper\Data $myModuleHelper, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \FME\Contactus\Model\ContactFactory $_contactModel)
    {
        $this->___init();
        parent::__construct($context, $formKeyValidator, $storeManager, $customerRepository, $subscriberFactory, $myModuleHelper, $transportBuilder, $_contactModel);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
