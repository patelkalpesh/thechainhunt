<?php
namespace Aheadworks\Layerednav\Api\Data;

/**
 * Extension class for @see \Aheadworks\Layerednav\Api\Data\FilterCategoryInterface
 */
class FilterCategoryExtension extends \Magento\Framework\Api\AbstractSimpleObject implements FilterCategoryExtensionInterface
{
}
