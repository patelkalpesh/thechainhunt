<?php
namespace Aheadworks\Layerednav\Api\Data;

/**
 * ExtensionInterface class for @see \Aheadworks\Layerednav\Api\Data\FilterCategoryInterface
 */
interface FilterCategoryExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
}
