<?php
namespace Aheadworks\Layerednav\Api\Data;

/**
 * ExtensionInterface class for @see \Aheadworks\Layerednav\Api\Data\StoreValueInterface
 */
interface StoreValueExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
}
