<?php
namespace Aheadworks\Layerednav\Api\Data;

/**
 * Extension class for @see \Aheadworks\Layerednav\Api\Data\StoreValueInterface
 */
class StoreValueExtension extends \Magento\Framework\Api\AbstractSimpleObject implements StoreValueExtensionInterface
{
}
