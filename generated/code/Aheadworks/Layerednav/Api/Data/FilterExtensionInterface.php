<?php
namespace Aheadworks\Layerednav\Api\Data;

/**
 * ExtensionInterface class for @see \Aheadworks\Layerednav\Api\Data\FilterInterface
 */
interface FilterExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
}
