<?php
namespace Aheadworks\Layerednav\Api\Data;

/**
 * Extension class for @see \Aheadworks\Layerednav\Api\Data\FilterInterface
 */
class FilterExtension extends \Magento\Framework\Api\AbstractSimpleObject implements FilterExtensionInterface
{
}
