<?php
namespace Aheadworks\Layerednav\Controller\Adminhtml\Filter\MassChangeStatusInSearch;

/**
 * Interceptor class for @see \Aheadworks\Layerednav\Controller\Adminhtml\Filter\MassChangeStatusInSearch
 */
class Interceptor extends \Aheadworks\Layerednav\Controller\Adminhtml\Filter\MassChangeStatusInSearch implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Aheadworks\Layerednav\Model\ResourceModel\Filter\CollectionFactory $filterCollectionFactory, \Aheadworks\Layerednav\Api\FilterRepositoryInterface $filterRepository)
    {
        $this->___init();
        parent::__construct($context, $filter, $filterCollectionFactory, $filterRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
