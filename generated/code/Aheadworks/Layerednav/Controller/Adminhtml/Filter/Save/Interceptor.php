<?php
namespace Aheadworks\Layerednav\Controller\Adminhtml\Filter\Save;

/**
 * Interceptor class for @see \Aheadworks\Layerednav\Controller\Adminhtml\Filter\Save
 */
class Interceptor extends \Aheadworks\Layerednav\Controller\Adminhtml\Filter\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Aheadworks\Layerednav\Api\Data\FilterInterfaceFactory $filterFactory, \Aheadworks\Layerednav\Api\FilterRepositoryInterface $filterRepository, \Aheadworks\Layerednav\Api\Data\FilterCategoryInterfaceFactory $filterCategoryFactory, \Magento\Framework\Api\DataObjectHelper $dataObjectHelper, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, \Aheadworks\Layerednav\Model\Filter\PostDataProcessor $filterPostDataProcessor)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $filterFactory, $filterRepository, $filterCategoryFactory, $dataObjectHelper, $dataPersistor, $filterPostDataProcessor);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
