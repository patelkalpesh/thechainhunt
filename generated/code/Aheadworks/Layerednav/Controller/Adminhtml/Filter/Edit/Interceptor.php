<?php
namespace Aheadworks\Layerednav\Controller\Adminhtml\Filter\Edit;

/**
 * Interceptor class for @see \Aheadworks\Layerednav\Controller\Adminhtml\Filter\Edit
 */
class Interceptor extends \Aheadworks\Layerednav\Controller\Adminhtml\Filter\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Aheadworks\Layerednav\Api\FilterRepositoryInterface $filterRepository)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $filterRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
