<?php
namespace Aheadworks\ShopByBrand\Api\Data;

/**
 * Extension class for @see \Aheadworks\ShopByBrand\Api\Data\BrandContentInterface
 */
class BrandContentExtension extends \Magento\Framework\Api\AbstractSimpleObject implements BrandContentExtensionInterface
{
}
