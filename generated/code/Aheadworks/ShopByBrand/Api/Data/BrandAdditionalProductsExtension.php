<?php
namespace Aheadworks\ShopByBrand\Api\Data;

/**
 * Extension class for @see \Aheadworks\ShopByBrand\Api\Data\BrandAdditionalProductsInterface
 */
class BrandAdditionalProductsExtension extends \Magento\Framework\Api\AbstractSimpleObject implements BrandAdditionalProductsExtensionInterface
{
}
