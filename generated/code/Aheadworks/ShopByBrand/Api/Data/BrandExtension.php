<?php
namespace Aheadworks\ShopByBrand\Api\Data;

/**
 * Extension class for @see \Aheadworks\ShopByBrand\Api\Data\BrandInterface
 */
class BrandExtension extends \Magento\Framework\Api\AbstractSimpleObject implements BrandExtensionInterface
{
}
