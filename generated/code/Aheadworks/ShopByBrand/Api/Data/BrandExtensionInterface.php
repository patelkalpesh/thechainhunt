<?php
namespace Aheadworks\ShopByBrand\Api\Data;

/**
 * ExtensionInterface class for @see \Aheadworks\ShopByBrand\Api\Data\BrandInterface
 */
interface BrandExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
}
