<?php
namespace Aheadworks\ShopByBrand\Api\Data;

/**
 * ExtensionInterface class for @see \Aheadworks\ShopByBrand\Api\Data\BrandAdditionalProductsInterface
 */
interface BrandAdditionalProductsExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
}
