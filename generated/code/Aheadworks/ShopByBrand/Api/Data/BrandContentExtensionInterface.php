<?php
namespace Aheadworks\ShopByBrand\Api\Data;

/**
 * ExtensionInterface class for @see \Aheadworks\ShopByBrand\Api\Data\BrandContentInterface
 */
interface BrandContentExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
}
