<?php
namespace Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Edit;

/**
 * Interceptor class for @see \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Edit
 */
class Interceptor extends \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Aheadworks\ShopByBrand\Api\Data\BrandInterfaceFactory $brandFactory, \Aheadworks\ShopByBrand\Api\BrandRepositoryInterface $brandRepository, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $brandFactory, $brandRepository, $coreRegistry, $dataObjectProcessor, $dataPersistor);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
