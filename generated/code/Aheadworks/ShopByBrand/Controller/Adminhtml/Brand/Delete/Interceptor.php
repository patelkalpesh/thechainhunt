<?php
namespace Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Delete;

/**
 * Interceptor class for @see \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Delete
 */
class Interceptor extends \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Delete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Aheadworks\ShopByBrand\Api\BrandRepositoryInterface $brandRepository)
    {
        $this->___init();
        parent::__construct($context, $brandRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
