<?php
namespace Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Save;

/**
 * Interceptor class for @see \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Save
 */
class Interceptor extends \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, \Aheadworks\ShopByBrand\Model\Brand\PostDataProcessor\Composite $postDataProcessor, \Aheadworks\ShopByBrand\Api\BrandRepositoryInterface $brandRepository, \Aheadworks\ShopByBrand\Api\Data\BrandInterfaceFactory $brandFactory, \Magento\Framework\Api\DataObjectHelper $dataObjectHelper)
    {
        $this->___init();
        parent::__construct($context, $dataPersistor, $postDataProcessor, $brandRepository, $brandFactory, $dataObjectHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
