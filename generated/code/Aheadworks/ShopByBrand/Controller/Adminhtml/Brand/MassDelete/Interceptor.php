<?php
namespace Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\MassDelete;

/**
 * Interceptor class for @see \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\MassDelete
 */
class Interceptor extends \Aheadworks\ShopByBrand\Controller\Adminhtml\Brand\MassDelete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Aheadworks\ShopByBrand\Model\ResourceModel\Brand\CollectionFactory $collectionFactory, \Aheadworks\ShopByBrand\Api\BrandRepositoryInterface $brandRepository)
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory, $brandRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
