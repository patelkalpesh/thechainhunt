<?php
namespace Aheadworks\ShopByBrand\Controller\Brand\View;

/**
 * Interceptor class for @see \Aheadworks\ShopByBrand\Controller\Brand\View
 */
class Interceptor extends \Aheadworks\ShopByBrand\Controller\Brand\View implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Aheadworks\ShopByBrand\Api\BrandRepositoryInterface $brandRepository, \Magento\Catalog\Model\Layer\Resolver $layerResolver, \Magento\Store\Model\StoreManagerInterface $storeManager, \Aheadworks\ShopByBrand\Model\Config $config, \Aheadworks\ShopByBrand\Model\Brand\PageConfig $brandPageConfig)
    {
        $this->___init();
        parent::__construct($context, $brandRepository, $layerResolver, $storeManager, $config, $brandPageConfig);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
