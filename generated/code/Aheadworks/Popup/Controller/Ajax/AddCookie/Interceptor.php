<?php
namespace Aheadworks\Popup\Controller\Ajax\AddCookie;

/**
 * Interceptor class for @see \Aheadworks\Popup\Controller\Ajax\AddCookie
 */
class Interceptor extends \Aheadworks\Popup\Controller\Ajax\AddCookie implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Customer\Model\Session $customerSession, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator, \Magento\Framework\App\Action\Context $context, \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager, \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory, \Aheadworks\Popup\Model\PopupFactory $popupModelFactory)
    {
        $this->___init();
        parent::__construct($customerSession, $formKeyValidator, $context, $cookieManager, $cookieMetadataFactory, $popupModelFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
