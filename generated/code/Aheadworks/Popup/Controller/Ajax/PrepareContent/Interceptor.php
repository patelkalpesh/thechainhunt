<?php
namespace Aheadworks\Popup\Controller\Ajax\PrepareContent;

/**
 * Interceptor class for @see \Aheadworks\Popup\Controller\Ajax\PrepareContent
 */
class Interceptor extends \Aheadworks\Popup\Controller\Ajax\PrepareContent implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Customer\Model\Session $customerSession, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator, \Magento\Framework\App\Action\Context $context, \Aheadworks\Popup\Model\Config $config)
    {
        $this->___init();
        parent::__construct($customerSession, $formKeyValidator, $context, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
