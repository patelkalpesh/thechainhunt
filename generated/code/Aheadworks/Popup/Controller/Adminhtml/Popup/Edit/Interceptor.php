<?php
namespace Aheadworks\Popup\Controller\Adminhtml\Popup\Edit;

/**
 * Interceptor class for @see \Aheadworks\Popup\Controller\Adminhtml\Popup\Edit
 */
class Interceptor extends \Aheadworks\Popup\Controller\Adminhtml\Popup\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Registry $registry, \Aheadworks\Popup\Model\PopupFactory $popupModelFactory)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $registry, $popupModelFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
