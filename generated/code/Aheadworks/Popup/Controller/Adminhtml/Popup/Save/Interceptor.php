<?php
namespace Aheadworks\Popup\Controller\Adminhtml\Popup\Save;

/**
 * Interceptor class for @see \Aheadworks\Popup\Controller\Adminhtml\Popup\Save
 */
class Interceptor extends \Aheadworks\Popup\Controller\Adminhtml\Popup\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Aheadworks\Popup\Model\PopupFactory $popupModelFactory)
    {
        $this->___init();
        parent::__construct($context, $popupModelFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
