<?php
namespace Amasty\ElasticSearch\Controller\Adminhtml\Synonym\Edit;

/**
 * Interceptor class for @see \Amasty\ElasticSearch\Controller\Adminhtml\Synonym\Edit
 */
class Interceptor extends \Amasty\ElasticSearch\Controller\Adminhtml\Synonym\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, \Amasty\ElasticSearch\Model\SynonymRepository $synonymRepository, \Amasty\ElasticSearch\Model\SynonymFactory $synonymFactory, \Magento\Framework\Registry $registry)
    {
        $this->___init();
        parent::__construct($context, $resultForwardFactory, $synonymRepository, $synonymFactory, $registry);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
