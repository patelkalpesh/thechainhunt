<?php
namespace Amasty\ElasticSearch\Controller\Adminhtml\Stopword\Import;

/**
 * Interceptor class for @see \Amasty\ElasticSearch\Controller\Adminhtml\Stopword\Import
 */
class Interceptor extends \Amasty\ElasticSearch\Controller\Adminhtml\Stopword\Import implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, \Amasty\ElasticSearch\Model\StopWordRepository $stopWordRepository, \Amasty\ElasticSearch\Model\StopWordFactory $stopWordFactory, \Magento\Framework\Registry $registry)
    {
        $this->___init();
        parent::__construct($context, $resultForwardFactory, $stopWordRepository, $stopWordFactory, $registry);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
