<?php
namespace Amasty\Xsearch\Model\System\Config\Backend\Weight;

/**
 * Interceptor class for @see \Amasty\Xsearch\Model\System\Config\Backend\Weight
 */
class Interceptor extends \Amasty\Xsearch\Model\System\Config\Backend\Weight implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository, \Amasty\Xsearch\Helper\Data $xSearchHelper, \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $config, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Framework\Math\Random $mathRandom, \Magento\Catalog\Model\ResourceModel\Attribute $attributeResource, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = array())
    {
        $this->___init();
        parent::__construct($attributeRepository, $xSearchHelper, $context, $registry, $config, $cacheTypeList, $mathRandom, $attributeResource, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'afterSave');
        if (!$pluginInfo) {
            return parent::afterSave();
        } else {
            return $this->___callPlugins('afterSave', func_get_args(), $pluginInfo);
        }
    }
}
