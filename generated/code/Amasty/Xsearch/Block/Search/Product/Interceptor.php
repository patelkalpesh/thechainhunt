<?php
namespace Amasty\Xsearch\Block\Search\Product;

/**
 * Interceptor class for @see \Amasty\Xsearch\Block\Search\Product
 */
class Interceptor extends \Amasty\Xsearch\Block\Search\Product implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Block\Product\Context $context, \Magento\Framework\Data\Helper\PostHelper $postDataHelper, \Magento\Catalog\Model\Layer\Resolver $layerResolver, \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository, \Magento\Framework\Url\Helper\Data $urlHelper, \Magento\Framework\Stdlib\StringUtils $string, \Magento\Framework\Data\Form\FormKey $formKey, \Amasty\Xsearch\Helper\Data $xSearchHelper, \Magento\Framework\App\Response\RedirectInterface $redirector, array $data = array())
    {
        $this->___init();
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $string, $formKey, $xSearchHelper, $redirector, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getImage($product, $imageId, $attributes = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getImage');
        if (!$pluginInfo) {
            return parent::getImage($product, $imageId, $attributes);
        } else {
            return $this->___callPlugins('getImage', func_get_args(), $pluginInfo);
        }
    }
}
