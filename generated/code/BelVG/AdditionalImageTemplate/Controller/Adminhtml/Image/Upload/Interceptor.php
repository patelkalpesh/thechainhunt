<?php
namespace BelVG\AdditionalImageTemplate\Controller\Adminhtml\Image\Upload;

/**
 * Interceptor class for @see \BelVG\AdditionalImageTemplate\Controller\Adminhtml\Image\Upload
 */
class Interceptor extends \BelVG\AdditionalImageTemplate\Controller\Adminhtml\Image\Upload implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \BelVG\AdditionalImageTemplate\Model\ImageUploader $imageUploader)
    {
        $this->___init();
        parent::__construct($context, $imageUploader);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
