<?php

namespace Evincemage\Customcheckout\Plugin;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;

class Redirect
{
    protected $coreRegistry;

    protected $url;

    protected $resultFactory;

    public function __construct(Registry $registry, UrlInterface $url, ResultFactory $resultFactory)
    {
        $this->coreRegistry = $registry;
        $this->url = $url;
        $this->resultFactory = $resultFactory;
    }

    public function aroundGetRedirect ($subject, \Closure $proceed)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
            $customerSession = $objectManager->get('Magento\Framework\Session\SessionManagerInterface');
        if ($this->coreRegistry->registry('is_new_account') && $customerSession->getFromcheckout()) {
            /** @var \Magento\Framework\Controller\Result\Redirect $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $result->setUrl($this->url->getUrl('checkout'));
            return $result;
        }

        return $proceed();
    }
}