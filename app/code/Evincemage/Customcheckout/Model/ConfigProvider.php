<?php

namespace Evincemage\Customcheckout\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\LayoutInterface;

class ConfigProvider implements ConfigProviderInterface
{
   /** @var LayoutInterface  */
   protected $_layout;
   protected $cmsBlock;
 
   public function __construct(LayoutInterface $layout, $blockId,\Magento\Framework\Session\SessionManagerInterface $coreSession)
   {
       $this->_layout = $layout;
       $this->cmsBlock = $this->constructBlock($blockId);
       $this->_coreSession = $coreSession;
   }
 
   public function constructBlock($blockId){
       $block = $this->_layout->createBlock('Magento\Cms\Block\Block')
           ->setBlockId($blockId)->toHtml();
       return $block;
   }
 
   public function getConfig()
   {
       return [
           'cms_block' => $this->cmsBlock,
           'guestemail' => $this->_coreSession->getCheckoutemail()
       ];
   }
}