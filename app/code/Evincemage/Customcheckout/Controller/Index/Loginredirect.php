<?php

namespace Evincemage\Customcheckout\Controller\Index;

use Magento\Framework\Controller\ResultFactory; 

class Loginredirect extends \Magento\Framework\App\Action\Action
{

    protected $request;

    protected $customermodel;

    protected $_registry;

    protected $customerSession;

    protected $_coreSession;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->request = $request;
        $this->_registry = $registry;
        $this->_coreSession = $coreSession;
        $this->_storeManager=$storeManager;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
        $this->_coreSession->start();
        $this->_coreSession->unsFromcheckout();
        $this->_coreSession->setFromcheckout('redirect to checkout!!');
        /*$objectManager = \Magento\Framework\App\ObjectManager::getInstance();*/
        $url = $this->_storeManager->getStore()->getBaseUrl().'checkout/';
        $redirectionUrl = $this->_storeManager->getStore()->getBaseUrl().'customer/account/login/referer/'.base64_encode($url);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setUrl($redirectionUrl);
    }
}
