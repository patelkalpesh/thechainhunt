<?php

namespace Evincemage\Customcheckout\Controller\Index;

use Magento\Framework\Controller\ResultFactory; 

class Guestcheckout extends \Magento\Framework\App\Action\Action
{

    protected $request;

    protected $customermodel;

    protected $_registry;

    protected $customerSession;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->request = $request;
        $this->_registry = $registry;
         $this->_storeManager=$storeManager;
         $this->_coreSession = $coreSession;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
        $data = $this->getRequest()->getPostValue();
        $encodedemail = $data['login']['username'];
        $this->_coreSession->start();
        $this->_coreSession->unsCheckoutemail();
        $this->_coreSession->setCheckoutemail($encodedemail);
        $url = $this->_storeManager->getStore()->getBaseUrl().'checkout/';
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setUrl($url);
    }
}
