<?php
/**
 * Copyright © 2017 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageWorx\ShippingRules\Model\Carrier;

class AbstractModel extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Columns which will be ignored during import/export process
     * @see \MageWorx\ShippingRules\Model\Carrier\AbstractModel::getIgnoredColumnsForImportExport()
     */
    const IMPORT_EXPORT_IGNORE_COLUMNS = [
        'id'
    ];

    /**
     * Get columns which should be removed during import\export process
     *
     * @return array
     */
    public static function getIgnoredColumnsForImportExport()
    {
        return static::IMPORT_EXPORT_IGNORE_COLUMNS;
    }

    /**
     * Convert current object to string
     *
     * @param string $format
     * @return mixed|string
     */
    public function toString($format = '')
    {
        if (empty($format)) {
            $result = implode(', ', $this->getData());
        } else {
            preg_match_all('/\{\{([a-z0-9_]+)\}\}/is', $format, $matches);
            foreach ($matches[1] as $var) {
                $method = 'get' . implode('', array_map('ucfirst', explode('_', $var)));
                if (method_exists($this, $method)) {
                    $data = $this->{$method}();
                } else {
                    $data = $this->getData($var);
                }

                // Format array values
                if (is_array($data)) {
                    $formattedData = json_encode(
                        $data,
                        JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK
                    );
                } else {
                    $formattedData = $data;
                }

                $formattedData = str_ireplace(',"', '`"', $formattedData);

                $format = str_replace('{{' . $var . '}}', $formattedData, $format);
            }
            $result = $format;
        }

        return $result;
    }
}