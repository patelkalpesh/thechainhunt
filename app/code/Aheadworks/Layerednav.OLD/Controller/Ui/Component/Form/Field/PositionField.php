<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Layerednav\Ui\Component\Form\Field;

use Magento\Ui\Component\Form\Field as FormField;

/**
 * Class PositionField
 * @package Aheadworks\Layerednav\Ui\Component\Form\Field
 */
class PositionField extends FormField
{
    /**
     * {@inheritdoc}
     */
    public function prepare()
    {
        $storeId = $this->getContext()->getRequestParam('store');
        if ($storeId) {
            $config = $this->getConfig();
            $config['visible'] = false;
            $this->setConfig($config);
        }

        parent::prepare();
    }
}
