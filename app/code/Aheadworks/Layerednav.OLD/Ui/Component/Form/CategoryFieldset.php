<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\Layerednav\Ui\Component\Form;

use Magento\Ui\Component\Form\Fieldset as FormFieldset;

/**
 * Class CategoryFieldset
 * @package Aheadworks\Layerednav\Ui\Component\Form
 */
class CategoryFieldset extends FormFieldset
{
    /**
     * {@inheritdoc}
     */
    public function prepare()
    {
        $storeId = $this->getContext()->getRequestParam('store');
        if ($storeId) {
            $config = $this->getConfig();
            $config['visible'] = false;
            $this->setConfig($config);
        }

        parent::prepare();
    }
}
