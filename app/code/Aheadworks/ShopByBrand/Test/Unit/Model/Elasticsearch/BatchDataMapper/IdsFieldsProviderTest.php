<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\ShopByBrand\Test\Unit\Model\Elasticsearch\BatchDataMapper;

use Aheadworks\ShopByBrand\Model\Elasticsearch\BatchDataMapper\IdsFieldsProvider;
use PHPUnit\Framework\TestCase;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Test for \Aheadworks\ShopByBrand\Model\Elasticsearch\BatchDataMapper\IdsFieldsProvider
 */
class IdsFieldsProviderTest extends TestCase
{
    /**
     * @var IdsFieldsProvider
     */
    private $idsFieldsProvider;

    /**
     * Init mocks for tests
     *
     * @return void
     */
    public function setUp()
    {
        $objectManager = new ObjectManager($this);

        $this->idsFieldsProvider = $objectManager->getObject(IdsFieldsProvider::class, []);
    }

    /**
     * Test getFields method
     * @param int[] $productIds
     * @param int $storeId
     * @param array $result
     * @dataProvider getFieldsDataProvider
     */
    public function testgetFields($productIds, $storeId, $result)
    {
        $this->assertEquals($result, $this->idsFieldsProvider->getFields($productIds, $storeId));
    }

    /**
     * @return array
     */
    public function getFieldsDataProvider()
    {
        return [
            [
                'productIds' => [],
                'storeId' => 1,
                'result' => []
            ],
            [
                'productIds' => [10],
                'storeId' => 1,
                'result' => [
                    10 => ['entity_id' => 10]
                ]
            ],
            [
                'productIds' => [10, 11, 12],
                'storeId' => 1,
                'result' => [
                    10 => ['entity_id' => 10],
                    11 => ['entity_id' => 11],
                    12 => ['entity_id' => 12]
                ]
            ],
        ];
    }
}
