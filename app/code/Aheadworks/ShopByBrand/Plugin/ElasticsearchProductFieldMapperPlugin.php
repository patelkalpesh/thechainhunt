<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\ShopByBrand\Plugin;

use Magento\Elasticsearch\Model\Adapter\FieldMapper\ProductFieldMapper as ElasticsearchProductFieldMapper;
use Magento\Elasticsearch\Elasticsearch5\Model\Adapter\FieldMapper\ProductFieldMapper
    as Elasticsearch5ProductFieldMapper;

/**
 * Class ElasticsearchProductFieldMapperPlugin
 * @package Aheadworks\ShopByBrand\Plugin
 */
class ElasticsearchProductFieldMapperPlugin
{
    /**
     * Push entity_id to index config
     *
     * @param Elasticsearch5ProductFieldMapper|ElasticsearchProductFieldMapper $subject
     * @param array $result
     * @return array
     */
    public function afterGetAllAttributesTypes($subject, $result)
    {
        $result['entity_id'] = ['type' => 'integer'];

        return $result;
    }
}
