<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Aheadworks\ShopByBrand\Model\Elasticsearch\BatchDataMapper;

/**
 * Class IdsFieldsProvider
 * @package Aheadworks\ShopByBrand\Model\Elasticsearch\BatchDataMapper
 */
class IdsFieldsProvider
{
    /**
     * Get additional fields for data mapper
     *
     * @param array $productIds
     * @param int $storeId
     * @return array
     */
    public function getFields(array $productIds, $storeId)
    {
        $fields = [];
        foreach ($productIds as $productId) {
            $fields[$productId] = ['entity_id' => $productId];
        }

        return $fields;
    }
}
