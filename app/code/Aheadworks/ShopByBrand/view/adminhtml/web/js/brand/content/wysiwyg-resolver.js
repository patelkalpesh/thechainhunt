/**
* Copyright 2019 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

define([
    'mage/adminhtml/wysiwyg/tiny_mce/setup',
    'mage/adminhtml/wysiwyg/widget'
], function () {
    'use strict';

    return {
        /**
         * Get wysiwyg object
         *
         * @param {*} htmlId
         * @param {Object} config
         * @returns {Object}
         */
        getWysiwyg: function(htmlId, config) {
            var wysiwyg;

            if (window.wysiwygSetup) {
                wysiwyg = new wysiwygSetup(htmlId, config);
            } else {
                wysiwyg = new tinyMceWysiwygSetup(htmlId, config);
            }

            return wysiwyg;
        },
    };
});
