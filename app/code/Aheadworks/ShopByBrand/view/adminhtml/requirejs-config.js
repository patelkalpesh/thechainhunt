/**
* Copyright 2019 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

var config = {
    map: {
        '*': {
            awSbbContent: 'Aheadworks_ShopByBrand/js/brand/content'
        }
    }
};
