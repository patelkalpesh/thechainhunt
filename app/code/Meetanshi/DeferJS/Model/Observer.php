<?php

namespace Meetanshi\DeferJS\Model;

use Meetanshi\DeferJS\Helper\Data;

class Observer implements \Magento\Framework\Event\ObserverInterface
{
    private $helper;

    public function __construct(
        Data $helper
    )
    {

        $this->helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getEvent()->getData('request');
        if (!$this->helper->isEnabled($request)) {
            return;
        }
        $response = $observer->getEvent()->getData('response');
        if (!$response) {
            return;
        }
        $html = $response->getBody();
        if ($html == '') {
            return;
        }

        $conditionalJsPattern = '@(?:<script type="text/javascript"|<script)(.*)</script>@msU';
        preg_match_all($conditionalJsPattern, $html, $_matches);

        foreach ($_matches[0] as $key => $jsCode) {
            if (strpos($jsCode, 'googletagmanager.com') !== false) {
                unset($_matches[0][$key]);
            } else {
                $html = str_replace($jsCode, "", $html);
            }
        }

        $_js_if = implode('', $_matches[0]);
        //$html = preg_replace($conditionalJsPattern, '', $html);
        $html .= $_js_if;

        $response->setBody($html);
    }
}
