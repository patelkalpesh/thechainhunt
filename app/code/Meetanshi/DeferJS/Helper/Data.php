<?php

namespace Meetanshi\DeferJS\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Data extends AbstractHelper
{
    private $productMetadataInterface;
    private $serializerInterface;

    public function __construct(
        Context $context,
        ProductMetadataInterface $productMetadataInterface,
        SerializerInterface $serializerInterface
    )
    {

        $this->scopeConfig = $context->getScopeConfig();
        $this->productMetadataInterface = $productMetadataInterface;
        $this->serializerInterface = $serializerInterface;
        parent::__construct($context);
    }

    public function isEnabled($request)
    {
        $active = $this->scopeConfig->getValue(
            'deferjs/general/active',
            ScopeInterface::SCOPE_STORE
        );
        if ($active != 1) {
            return false;
        }
        $active = $this->scopeConfig->getValue(
            'deferjs/general/home_page',
            ScopeInterface::SCOPE_STORE
        );
        if ($active == 1 && $request->getFullActionName() == 'cms_index_index') {
            return false;
        }
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();
        if ($this->regexMatchSimple($this->scopeConfig->getValue(
            'deferjs/general/controller',
            ScopeInterface::SCOPE_STORE
        ), "{$module}_{$controller}_{$action}", 1)) {
            return false;
        }
        if ($this->regexMatchSimple($this->scopeConfig->getValue(
            'deferjs/general/path',
            ScopeInterface::SCOPE_STORE
        ), $request->getRequestUri(), 2)) {
            return false;
        }
        return true;
    }

    public function regexMatchSimple($regex, $matchTerm, $type)
    {
        if (!$regex) {
            return false;
        }

        $version = $this->productMetadataInterface->getVersion();
        if ($version >= '2.2.0') {
            $rules = $this->serializerInterface->unserialize($regex);
        } else {
            $rules = (array)unserialize($regex);
        }

        if (empty($rules)) {
            return false;
        }
        foreach ($rules as $rule) {
            $regex = trim($rule['defer'], '#');
            if ($regex == '') {
                continue;
            }
            if ($type == 1) {
                $regexs = explode('_', $regex);
                switch (count($regexs)) {
                    case 1:
                        $regex = $regex . '_index_index';
                        break;
                    case 2:
                        $regex = $regex . '_index';
                        break;
                    default:
                        break;
                }
            }
            $regexp = '#' . $regex . '#';
            if (@preg_match($regexp, $matchTerm)) {
                return true;
            }
        }
        return false;
    }
}
