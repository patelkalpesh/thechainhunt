<?php

/**
 * MagePrince
 * Copyright (C) 2018 Mageprince
 *
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://opensource.org/licenses/gpl-3.0.html
 *
 * @category MagePrince
 * @package Prince_Buynow
 * @copyright Copyright (c) 2018 MagePrince
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author MagePrince
 */

namespace Prince\Buynow\Controller\Cart;
use Magento\Framework\Controller\ResultFactory; 

class Add extends \Magento\Checkout\Controller\Cart\Add
{
    // protected $request;

    // protected $customermodel;

    // protected $_registry;

    // protected $customerSession;

    // protected $_coreSession;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    // public function __construct(
    //     \Magento\Framework\App\Action\Context $context,
    //     \Magento\Framework\Registry $registry,
    //     \Magento\Store\Model\StoreManagerInterface $storeManager,
    //     \Magento\Framework\Session\SessionManagerInterface $coreSession,
    //     \Magento\Framework\App\Request\Http $request
    // ) {
    //     $this->request = $request;
    //     $this->_registry = $registry;
    //     $this->_coreSession = $coreSession;
    //     $this->_storeManager=$storeManager;
    //     parent::__construct($context);
    // }

    /**
     * Add product to shopping cart action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            if (!$product) {
                return $this->goBack();
            }

            $cartProducts = $this->_objectManager->create('Prince\Buynow\Helper\Data')
                             ->getConfig('buynow/general/keep_cart_products');
            if (!$cartProducts) {
                $this->cart->truncate(); //remove all products from cart
            } 

            $this->cart->addProduct($product, $params);
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }

            $this->cart->save();
            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                $baseUrl = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface')
                            ->getStore()->getBaseUrl();
                //echo ($this->goBack($baseUrl.'checkout/', $product));exit;

                //Nimit added redirect logic here 31 Aug 2018
                //$this->_objectManager->get('Magento\Customer\Model\Session');

                error_reporting(E_ALL);
                ini_set("display_errors", 1);
                //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');

               
                if($customerSession->isLoggedIn()) 
                {
                   $nimitGoTo = $baseUrl.'checkout/';
                }
                else
                {
                   $_coreSession = $this->_objectManager->create('Magento\Framework\Session\SessionManagerInterface');
                    $_coreSession->start();
                    $_coreSession->unsFromcheckout();
                    $_coreSession->setFromcheckout('redirect to checkout!!');
                    $url = $this->_storeManager->getStore()->getBaseUrl().'checkout/';
                    $nimitGoTo = $this->_storeManager->getStore()->getBaseUrl().'customer/account/login/referer/'.base64_encode($url);
                    //$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    //$nimitGoTo = $resultRedirect->setUrl($redirectionUrl);
                    //print_r($redirectionUrl );exit;
                }
                //echo $nimitGoTo; exit;
                //return $this->goBack($baseUrl.'checkout/', $product);
                return $this->goBack($nimitGoTo, $product);
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice(
                    $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                );
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError(
                        $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($message)
                    );
                }
            }
            $url = $this->_checkoutSession->getRedirectUrl(true);
            if (!$url) {
                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
                $url = $this->_redirect->getRedirectUrl($cartUrl);
            }
            return $this->goBack($url);
        } catch (\Exception $e) {
           
            $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return $this->goBack();
        }
    }
}
