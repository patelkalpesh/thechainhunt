var config = {
    map: {
        '*': {
            MeigeeBootstrap: 'Meigee_Feather/js/bootstrap.min',
            //MeigeeCarousel: 'Meigee_Feather/js/owl.carousel',
            jqueryBackstretch: 'Meigee_Feather/js/jquery.backstretch.min',
			googleMap: 'https://maps.googleapis.com/maps/api/js?sensor=false',
//            lightBox: 'Meigee_Feather/js/ekko-lightbox.min'
           lightBox: 'Meigee_Feather/js/ekko-lightbox.customized',
		        sticky_menu: 'Meigee_Feather/js/sticky_menu',
           mobile_menu: 'Meigee_Feather/js/mobile_menu'
	   /*
	   //imagesloaded: 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js',
            imagesloaded: 'Meigee_Feather/js/imagesloaded.pkgd.min',
            //masonry: 'Meigee_Feather/js/masonry.pkgd',
            //masonry: 'https://unpkg.com/masonry-layout@4.2.0/dist/masonry.pkgd.js'
	   */
        }
    },
    /*
    "shim": {
        "masonry": ['jquery', 'jquery/ui'],
    }*/

};




