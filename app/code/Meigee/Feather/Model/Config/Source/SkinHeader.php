<?php
namespace Meigee\Feather\Model\Config\Source;

class SkinHeader implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
            ['value' => 'default.css', 'label' => __('Default'), 'img' => 'Meigee_Feather::images/default.jpg', 'header' => 'default'],
            ['value' => 'fashion.css', 'label' => __('Fashion'), 'img' => 'Meigee_Feather::images/fashion.jpg', 'header' => '2'],
            ['value' => 'electronics2.css', 'label' => __('Electronics 2'), 'img' => 'Meigee_Feather::images/electronics2.jpg'],
            ['value' => 'furniture.css', 'label' => __('Furniture'), 'img' => 'Meigee_Feather::images/furniture.jpg'],
            ['value' => 'clothes.css', 'label' => __('Clothes'), 'img' => 'Meigee_Feather::images/clothes.jpg'],
		];
    }
}
