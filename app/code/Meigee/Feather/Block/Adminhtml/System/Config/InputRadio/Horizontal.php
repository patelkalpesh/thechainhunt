<?php
namespace Meigee\Feather\Block\Adminhtml\System\Config\InputRadio;

class Horizontal extends \Meigee\Feather\Block\Adminhtml\System\Config\InputRadio
{
    function getTypeClass()
    {
        return 'meigee-thumb-horizontal';
    }
}
