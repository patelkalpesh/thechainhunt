<?php
namespace Meigee\Feather\Block\Adminhtml\System\Config\InputRadio;

class Vertical extends \Meigee\Feather\Block\Adminhtml\System\Config\InputRadio
{
    function getTypeClass()
    {
        return 'meigee-thumb-vertical';
    }
}
