<?php
namespace Meigee\Feather\Helper;

class FeatherGeneral extends Section
{
    protected function getThemeCnfBlock()
    {
        return 'feather_general';
    }
}
