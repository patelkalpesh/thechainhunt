<?php

namespace Meigee\Feather\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Store\Model\Store;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class FrontHelper extends \Magento\Framework\Url\Helper\Data
{

    /**
     * @var TimezoneInterface
     */
    protected $localeDate;
	protected $_request;
	protected $_registry;
	// protected $categoryFactory;

    public function __construct(
		// \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Registry $registry,
        TimezoneInterface $localeDate,
		array $data = []
    ) {
        $this->localeDate = $localeDate;
		$this->_request = $request;
		$this->_registry = $registry;
		// $this->categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    public function isProductNew(ModelProduct $product)
    {
        $newsFromDate = $product->getNewsFromDate();
        $newsToDate = $product->getNewsToDate();
        if (!$newsFromDate && !$newsToDate) {
            return false;
        }

        return $this->localeDate->isScopeDateInInterval(
            $product->getStore(),
            $newsFromDate,
            $newsToDate
        );
    }
	public function isProductSale(ModelProduct $product)
    {
		$finalPrice = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
		$regularPrice = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();
		if($regularPrice != $finalPrice){
			return true;
		} else {
			return false;
		}
    }

	public function getCurrentPage()
	{
		return $this->_request->getFullActionName();
	}
}