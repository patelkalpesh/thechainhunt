<?php
namespace Meigee\Feather\Helper;

class FeatherBgSlider extends Section
{
    protected function getThemeCnfBlock()
    {
        return 'feather_bg_slider';
    }
}
