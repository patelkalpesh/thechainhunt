/*
 * *
 *  * Copyright © 2018 www.evincemage.com, Inc. All rights reserved.
 *
 */

/**
 * @api
 */
define([
    'jquery',
    '../model/quote'
], function ($, quote) {
    'use strict';

    return function (billingAddress) {
        quote.billingAddress(billingAddress);
    };
});
