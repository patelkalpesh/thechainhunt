/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator'
], function (Component, quote, stepNavigator) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/summary/grand-total'
        },

        /**
         * @return {*}
         */
        isDisplayed: function () {
            return this.isFullMode();
        },

        /**
         * Get pure value.
         */
        getPureValue: function () {
            var totals = quote.getTotals()();
            if (totals) {
                return totals['grand_total'];
            }
            return quote['grand_total'];
        },

        /**
         * @return {*|String}
         */
        getValue: function () {
            var totals = quote.getTotals()();
           // console.log('before');
            if(typeof(quote.shippingMethod()) != "undefined" && quote.shippingMethod() !== null) {
            var shippingMethod = quote.shippingMethod();
            var price =  shippingMethod.amount;
        }
            var plus = "";
            var minus = "";
        if(totals['base_shipping_amount'] < price){
            plus = totals['base_shipping_amount'] + price;
          //  console.log('+');
            return this.getFormattedPrice((this.getPureValue() + price));
        } else if(totals['base_shipping_amount'] > price){
            minus = totals['base_shipping_amount'] - price;
            //console.log('-');
            return this.getFormattedPrice((this.getPureValue() - minus));
        }
        //console.log('here');
        return this.getFormattedPrice(this.getPureValue());
        }
    });
});
