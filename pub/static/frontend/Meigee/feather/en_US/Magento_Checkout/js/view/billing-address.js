/*
 * *
 *  * Copyright © 2018 www.evincemage.com, Inc. All rights reserved.
 *
 */

define([
        'ko',
        'underscore',
        'Magento_Ui/js/form/form',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Ui/js/model/messageList',
        'mage/translate',
        'mageUtils',
        'uiLayout',
        'Magento_Checkout/js/model/billing-address/form-popup-state',
        'Magento_Ui/js/modal/modal',
        'jquery',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
    ],
    function (
        ko,
        _,
        Component,
        customer,
        addressList,
        quote,
        createBillingAddress,
        selectBillingAddress,
        checkoutData,
        checkoutDataResolver,
        customerData,
        setBillingAddressAction,
        globalMessageList,
        $t,
        utils,
        layout,
        formPopUpState,
        modal,
        $,
        createShippingAddress,
        selectShippingAddress
    ) {
        'use strict';

        var lastSelectedBillingAddress = null,
            newAddressOption = {
                /**
                 * Get new address label
                 * @returns {String}
                 */
                getAddressInline: function () {
                    return $t('New Address');
                },
                customerAddressId: null
            },
            countryData = customerData.get('directory-data'),
            addressOptions = addressList().filter(function (address) {
                return address.getType() == 'customer-address'; //eslint-disable-line eqeqeq
            });
        var billingPopUp = null;

        var defaultBillingRendererTemplate = {
            parent: '${ $.$data.parentName }',
            name: '${ $.$data.name }',
            component: 'Magento_Checkout/js/view/billing-address/address-renderer/default'
        };

        addressOptions.push(newAddressOption);

        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/billing-address',
                shippingFormTemplate: 'Magento_Checkout/billing-address/form',
                rendererTemplates: [],
                popUpForm:{
                    element:'#opc-new-billing-address',
                    options:{
                        type:'popup',
                        responsive :'true',
                        innerScroll :'true',
                        title :'Billing Address',
                        trigger :'opc-new-billing-address',
                        buttons :{
                            save:{
                                text:$t('Save Address'),
                                class:'action primary action-save-address'
                            },
                            cancel:{
                                text:$t('Cancel'),
                                class:'action secondary action-hide-popup'
                            }
                        }
                    }
                }
            },
            currentBillingAddress: quote.billingAddress,
            addressOptions: addressOptions,
            customerHasAddresses: addressOptions.length > 1,
            isFormInline: addressList().length === 0,
            isBillingFormPopUpVisible: formPopUpState.isVisible,
            isNewBillingAddressAdded: ko.observable(false),

            /**
             * Init component
             */
            initialize: function () {
                var self = this,hasNewAddress;
                this._super().initChildren();
                quote.paymentMethod.subscribe(function () {
                    checkoutDataResolver.resolveBillingAddress();
                }, this);

                this.isBillingFormPopUpVisible.subscribe(function (value) {
                    if (value) {
                        self.getBillingPopUp().openModal();
                    }
                });
                hasNewAddress = addressList.some(function (address) {
                    return address.getType() == 'new-customer-address'; //eslint-disable-line eqeqeq
                });

                this.isNewBillingAddressAdded(hasNewAddress);

                addressList.subscribe(function (changes) {
                        var self = this;

                        changes.forEach(function (change) {
                            if (change.status === 'added') {
                                self.createRendererComponent(change.value, change.index);
                            }
                        });
                    },
                    this,
                    'arrayChange'
                );

            },
            /** @inheritdoc */
            initConfig: function () {
                this._super();
                // the list of child components that are responsible for address rendering
                this.rendererComponents = [];

                return this;
            },
            /** @inheritdoc */
            initChildren: function () {
                 _.each(addressList(), this.createRendererComponent, this);
                return this;
            },

            createRendererComponent: function (address, index) {

                var rendererTemplate, templateData, rendererComponent;

                if (index in this.rendererComponents) {
                    this.rendererComponents[index].address(address);
                } else {
                    // rendererTemplates are provided via layout
                    rendererTemplate = address.getType() != undefined && this.rendererTemplates[address.getType()] != undefined ? //eslint-disable-line
                        utils.extend({}, defaultBillingRendererTemplate, this.rendererTemplates[address.getType()]) :
                        defaultBillingRendererTemplate;
                    templateData = {
                        parentName: this.name,
                        name: index
                    };
                    rendererComponent = utils.template(rendererTemplate, templateData);
                    utils.extend(rendererComponent, {
                        address: ko.observable(address)
                    });
                    layout([rendererComponent]);
                    this.rendererComponents[index] = rendererComponent;
                }
            },
            /**
             * @return {exports.initObservable}
             */
            initObservable: function () {
                this._super()
                    .observe({
                        selectedAddress: null,
                        isAddressDetailsVisible: true,
                        isAddressFormVisible: !customer.isLoggedIn() || addressOptions.length === 1,
                        isAddressSameAsShipping: true,
                        saveInAddressBook: 1
                    });

                quote.billingAddress.subscribe(function (newAddress) {
                    if(this.isAddressFormVisible()) {
                        if (quote.isVirtual()) {
                            this.isAddressSameAsShipping(false);
                        } else {

                            this.isAddressSameAsShipping(
                                newAddress != null &&
                                newAddress.getCacheKey() == quote.shippingAddress().getCacheKey() //eslint-disable-line eqeqeq
                            );
                        }

                        if (newAddress != null && newAddress.saveInAddressBook !== undefined) {
                            this.saveInAddressBook(newAddress.saveInAddressBook);
                        } else {
                            this.saveInAddressBook(1);
                        }
                        this.isAddressDetailsVisible(true);
                    }
                }, this);

                return this;
            },

            canUseShippingAddress: ko.computed(function () {
                console.log('here');
                selectBillingAddress(quote.shippingAddress());
                return true;
            }),

            /**
             * @param {Object} address
             * @return {*}
             */
            addressOptionsText: function (address) {
                return address.getAddressInline();
            },

            /**
             * @return {Boolean}
             */
            useShippingAddress: function () {
                if (this.isAddressSameAsShipping()) {
                    selectBillingAddress(quote.shippingAddress());

                    this.updateAddresses();
                    this.isAddressDetailsVisible(true);
                } else {
                    lastSelectedBillingAddress = quote.billingAddress();
                    quote.billingAddress(null);
                    this.isAddressDetailsVisible(false);
                }
                checkoutData.setSelectedBillingAddress(null);

                return true;
            },

            /**
             * Update address action
             */
            updateAddress: function () {
                var addressData, newBillingAddress;

                if (this.selectedAddress() && this.selectedAddress() != newAddressOption) { //eslint-disable-line eqeqeq
                    selectBillingAddress(this.selectedAddress());
                    checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                } else {
                    this.source.set('params.invalid', false);
                    this.source.trigger(this.dataScopePrefix + '.data.validate');

                    if (this.source.get(this.dataScopePrefix + '.custom_attributes')) {
                        this.source.trigger(this.dataScopePrefix + '.custom_attributes.data.validate');
                    }

                    if (!this.source.get('params.invalid')) {
                        addressData = this.source.get(this.dataScopePrefix);

                        if (customer.isLoggedIn() && !this.customerHasAddresses) { //eslint-disable-line max-depth
                            this.saveInAddressBook(1);
                        }
                        addressData['save_in_address_book'] = this.saveInAddressBook() ? 1 : 0;
                        newBillingAddress = createBillingAddress(addressData);
                        // New address must be selected as a billing address
                        selectBillingAddress(newBillingAddress);
                        checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                        checkoutData.setNewCustomerBillingAddress(addressData);
                    }
                }
                this.updateAddresses();
            },

            /**
             * Edit address action
             */
            editAddress: function () {
                lastSelectedBillingAddress = quote.billingAddress();
                quote.billingAddress(null);
                this.isAddressDetailsVisible(false);
            },

            /**
             * Cancel address edit action
             */
            cancelAddressEdit: function () {
                this.restoreBillingAddress();

                if (quote.billingAddress()) {
                    // restore 'Same As Shipping' checkbox state
                    this.isAddressSameAsShipping(
                        quote.billingAddress() != null &&
                        quote.billingAddress().getCacheKey() == quote.shippingAddress().getCacheKey() && //eslint-disable-line
                        !quote.isVirtual()
                    );
                    this.isAddressDetailsVisible(true);
                }
            },

            /**
             * Restore billing address
             */
            restoreBillingAddress: function () {
                if (lastSelectedBillingAddress != null) {
                    selectBillingAddress(lastSelectedBillingAddress);
                }
            },

            /**
             * @param {Object} address
             */
            onAddressChange: function (address) {
                this.isAddressFormVisible(address == newAddressOption); //eslint-disable-line eqeqeq
            },

            /**
             * @param {Number} countryId
             * @return {*}
             */
            getCountryName: function (countryId) {
                return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
            },

            /**
             * Trigger action to update shipping and billing addresses
             */
            updateAddresses: function () {
                if (window.checkoutConfig.reloadOnBillingAddress ||
                    !window.checkoutConfig.displayBillingOnPaymentMethod
                ) {
                    setBillingAddressAction(globalMessageList);
                }
            },

            /**
             * Get code
             * @param {Object} parent
             * @returns {String}
             */
            getCode: function (parent) {
                return _.isFunction(parent.getCode) ? parent.getCode() : 'shared';
            },

            /**
             * @return {*}
             */
            getBillingPopUp: function () {
                var self = this,
                    buttons;

                if (!billingPopUp) {
                    buttons = this.popUpForm.options.buttons;
                    this.popUpForm.options.buttons = [
                        {
                            text: buttons.save.text ? buttons.save.text : $t('Save Address'),
                            class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
                            click: self.saveNewAddress.bind(self)
                        },
                        {
                            text: buttons.cancel.text ? buttons.cancel.text : $t('Cancel'),
                            class: buttons.cancel.class ? buttons.cancel.class : 'action secondary action-hide-popup',

                            /** @inheritdoc */
                            click: this.onClosePopUp.bind(this)
                        }
                    ];

                    /** @inheritdoc */
                    this.popUpForm.options.closed = function () {
                        self.isBillingFormPopUpVisible(false);
                    };

                    this.popUpForm.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
                    this.popUpForm.options.keyEventHandlers = {
                        escapeKey: this.onClosePopUp.bind(this)
                    };

                    /** @inheritdoc */
                    this.popUpForm.options.opened = function () {
                        // Store temporary address for revert action in case when user click cancel action
                        self.temporaryAddress = $.extend(true, {}, checkoutData.getShippingAddressFromData());
                    };

                    billingPopUp = modal(this.popUpForm.options, $(this.popUpForm.element));
                }

                return billingPopUp;
            },

            /**
             * Revert address and close modal.
             */
            onClosePopUp: function () {
                checkoutData.setShippingAddressFromData($.extend(true, {}, this.temporaryAddress));
                this.getBillingPopUp().closeModal();
            },

            /**
             * Show address form popup
             */
            showBillingFormPopUp: function () {
                this.isBillingFormPopUpVisible(true);
            },
            saveNewAddress: function () {
                var addressData,
                    newBillingAddress;

                this.source.set('params.invalid', false);
                this.triggerBillingDataValidateEvent();

                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get('billingAddressshared');

                    // if user clicked the checkbox, its value is true or false. Need to convert.
                    addressData['save_in_address_book'] = this.saveInAddressBook ? 1 : 0;

                    // New address must be selected as a shipping address
                    newBillingAddress = createBillingAddress(addressData);
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressData));
                    this.getBillingPopUp().closeModal();
                    this.isNewBillingAddressAdded(true);
                }
            },
            triggerBillingDataValidateEvent: function () {
                this.source.trigger('billingAddressshared.data.validate');

                if (this.source.get('billingAddressshared.custom_attributes')) {
                    this.source.trigger('billingAddressshared.custom_attributes.data.validate');
                }
            },
            /** Set selected customer billing address  */
            setMyBillingAddress: function () {
                setBillingAddressAction(this.address());
                checkoutData.setSelectedBillingAddress(this.address().getKey());
            },

        });
    });
