/*
 * *
 *  * Copyright © 2018 www.evincemage.com, Inc. All rights reserved.
 *
 */
define([
    'ko'
], function (ko) {
    'use strict';

    return {
        isVisible: ko.observable(false)
    };
});
