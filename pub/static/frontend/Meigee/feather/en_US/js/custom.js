jQuery(document).ready(function(){
    
    jQuery('.xzoom, .xzoom-gallery').xzoom({position: 'lens', lensShape: 'circle', bg:true, sourceClass: 'xzoom-hidden'});

    jQuery('#main_image').bind('click', function() {
        var xzoom = jQuery(this).data('xzoom');
        xzoom.closezoom();
        $.fancybox.open(xzoom.gallery().cgallery, {padding: 0, helpers: {overlay: {locked: false}}});
        event.preventDefault();
    });

    jQuery("#top_btn").click(function() {
        jQuery("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    });

    jQuery(window).scroll(function() {
        if (jQuery(document).scrollTop() > 20) {
            jQuery("#top_btn").css({"display": "block"});
            
        }
        else {
            jQuery("#top_btn").css({"display": "none"});
            
        }
    });
    
    jQuery('.tooltipped').tooltip();

    ///jQuery('.collapsible').collapsible();

    jQuery('select').formSelect();

    jQuery('.modal').modal();

    jQuery('#prod-carousel').customcarousel({
        fullWidth: true,
        indicators: true
    });

    var carHeight = jQuery('.carousel').height();

    var carbtnTop = (carHeight/2) -53;

    jQuery('#prod-carousel button').css({top: carbtnTop});

    jQuery('#prev_image').click( function() {
        jQuery('#prod-carousel').customcarousel('prev');
    });

    jQuery('#next_image').click( function() {
        jQuery('#prod-carousel').customcarousel('next');
    });

    jQuery('.materialboxed').materialbox();
   
    //jQuery('.sidenav').sidenav();
    //jQuery('.sidenav').isDragged

    jQuery('.sidenav').sidenav({ onCloseStart(){  jQuery(".sidenav-overlay").trigger('click'); }});

    //jQuery('.sidenav').sidenav({ onCloseEnd(){ jQuery(".button-list .action.primary.clear-button").trigger('click');  }});

    //onCloseEnd

    // jQuery(".button-list .action.primary.clear-button").trigger('click');
    jQuery('.collapsible').collapsible();

    ///
    jQuery('.collapsible4>li>.collapsible-header').click(function() {
    
        if( jQuery(this).parent().hasClass('active'))
        {
            jQuery(this).parent().removeClass('active');
            jQuery(this).next('.collapsible-body').slideUp(250);
        }
        else
        {
            jQuery(this).parent().parent().find('.active>.collapsible-body').slideUp(250);
            jQuery(this).parent().parent().find('li').removeClass('active');
            
            jQuery(this).parent().addClass('active');
            jQuery(this).next('.collapsible-body').slideDown(250);
        }
    });
    ///

    if (jQuery(window).width() < 420) {
        jQuery('.search-bar').css({"display": "flex"});
        jQuery('.search-bar-desktop').css({"display": "none"});
        var searchMobileWidth = jQuery('.search-mobile').width();
        var positionLeft = jQuery('.search-mobile').offset().left;
        var positionTop = jQuery('.search-mobile').offset().top;


        jQuery( ".search-mobile").change(function() {
            jQuery( ".search-popup").css({"display": "block"});
            jQuery('.add-to-bag-btn').css({"display": "none"});
            jQuery('.buy-now-btn').css({"display": "none"});

            jQuery(".nav-search").val(jQuery( ".search-mobile").val());
        });
    
        jQuery( "main").click(function() {
            jQuery( ".search-popup").css({"display": "none"});
            jQuery('.add-to-bag-btn').css({"display": "block"});
            jQuery('.buy-now-btn').css({"display": "block"});
        });

        if (jQuery(window).width() < 420) 
        {
            if(jQuery('#product_gallery').length)
            {
                jQuery( "#iammain" ).after( jQuery( "#product_gallery" ) );
            }
        }
        
    }
    else {
        
        jQuery('.search-bar').css({"display": "none"});
        jQuery('.search-bar-desktop').css({"display": "block"});

        if(jQuery('.nav-search').length)
        {
            var navSearchWidth = jQuery('.nav-search').width();
            var positionLeft = jQuery('.nav-search').offset().left;
            jQuery('.search-popup').width(navSearchWidth+13);
            //jQuery('.search-popup').css({"left": positionLeft});
            jQuery('.search-popup').css({"left": 12 });
            jQuery('.search-popup').css({"top": 41 });
        }


        jQuery(".nav-search").change(function() {
            jQuery( ".search-popup").css({"display": "block"});
        });

        jQuery("main").click(function() {
            jQuery( ".search-popup").css({"display": "none"});
        });
    }

    jQuery(".nav-popup").css({"width": "600px !important"});

    jQuery('input#city-autocomplete').autocomplete({
        data: {
          "London": null,
          "Manchester": null,
          "Liverpool": null
        },
        minLength: 0
    });

    jQuery('.cust_arrow').on("click", function() {
        jQuery('.checkout_arrow').removeClass("active");
        jQuery(this).addClass("active");
        jQuery('#customer_details_block').css({"display": "block"});
        jQuery('#delivery_options_block').css({"display": "none"});
        jQuery('#payment_details_block').css({"display": "none"});
    });

    jQuery('.del_arrow').on("click", function() {
        jQuery('.checkout_arrow').removeClass("active");
        jQuery(this).addClass("active");
        jQuery('#customer_details_block').css({"display": "none"});
        jQuery('#delivery_options_block').css({"display": "block"});
        jQuery('#payment_details_block').css({"display": "none"});
    });

    jQuery('.payment_arrow').on("click", function() {
        jQuery('.checkout_arrow').removeClass("active");
        jQuery(this).addClass("active");
        jQuery('#customer_details_block').css({"display": "none"});
        jQuery('#delivery_options_block').css({"display": "none"});
        jQuery('#payment_details_block').css({"display": "block"});
    });
    
    jQuery('#all_chains').mouseover(
        function() {
            jQuery('#nav_all').css({"display": "block"});
            jQuery('#nav_ladies').css({"display": "none"});
            jQuery('#nav_men').css({"display": "none"});
            jQuery('#nav_children').css({"display": "none"});
        }
    );

    jQuery('#ladies_chains').mouseover(
        function() {
            jQuery('#nav_all').css({"display": "none"});
            jQuery('#nav_ladies').css({"display": "block"});
            jQuery('#nav_men').css({"display": "none"});
            jQuery('#nav_children').css({"display": "none"});
        }
    );

    jQuery('#mens_chains').mouseover(
        function() {
            jQuery('#nav_all').css({"display": "none"});
            jQuery('#nav_ladies').css({"display": "none"});
            jQuery('#nav_men').css({"display": "block"});
            jQuery('#nav_children').css({"display": "none"});
        }
    );
    jQuery('#childrens_chains').mouseover(
        function() {
            jQuery('#nav_all').css({"display": "none"});
            jQuery('#nav_ladies').css({"display": "none"});
            jQuery('#nav_men').css({"display": "none"});
            jQuery('#nav_children').css({"display": "block"});
        }
    );


    jQuery(window).scroll(function() {

        if(jQuery('.quantity-row').length)
        {

            if ((jQuery(document).scrollTop() + jQuery(window).height()) > jQuery('.quantity-row').offset().top ) {
                jQuery('.add-to-bag-btn').addClass('sticky-cta-btn');
                jQuery('.buy-now-btn').addClass('sticky-cta-btn');
            }
            else {
                jQuery('.add-to-bag-btn').removeClass('sticky-cta-btn');
                jQuery('.buy-now-btn').removeClass('sticky-cta-btn');
                
            }
        }
    });

    var bagModal = 0;

    jQuery("#go_to_bag_modal").modal({
        endingTop: '40%'
    });

    jQuery(".edit-email").click( function() {
        jQuery("#customer_email").prop('disabled', false);
    });

    jQuery("#pay_checkout_btn").click(function() {
        jQuery(".checkout-section").css({"display": "none"});
        jQuery(".order-success-section").css({"display": "flex"});
    });

    

    jQuery('#blog_prev_image').click(function() {
        //jQuery('#blog_slider').customcarousel('prev');
    });
    jQuery('#blog_next_image').click(function() {
        //jQuery('#blog_slider').customcarousel('next');
    });

    jQuery('#prodcat_prev_image').click(function() {
        //jQuery('#prod_cat_slider').customcarousel('prev');
    });
    jQuery('#prodcat_next_image').click(function() {
        //jQuery('#prod_cat_slider').customcarousel('next');
    });


    
    


    jQuery('#login-tab').click( function () {
        jQuery('.switcher-tab').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('.login-form').css({"display": "block"});
        jQuery('.login-form.guest-form').css({"display": "none"});
    });
    jQuery('#guest-tab').click( function () {
        jQuery('.switcher-tab').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('.login-form').css({"display": "none"});
        jQuery('.login-form.guest-form').css({"display": "block"});
    });

    // disable materializecss select/dropdown
    jQuery('select').each(function(){
        var elem = document.getElementById(jQuery(this).attr('id'))
        var instance = M.FormSelect.getInstance(elem);
        if(instance)
        {
            instance.destroy();
        }
    });
    jQuery('.chain-length-btn').click( function () 
    {
        jQuery('.chain-length-btn').removeClass('active');

        if(jQuery(this).hasClass("active")) 
        {
            jQuery(this).removeClass('active');
        }
        else 
        {
            jQuery(this).addClass('active');
        }
    });
    // Model script
    jQuery(".custom-modal-trigger").click(function() {
        jQuery(jQuery(this).data("target")).css({"display": "flex"});
        jQuery("body").css({"overflow": "hidden"});
    });

    jQuery(".custom-modal-bg").click(function() {
        jQuery(".custom-modal").css({"display": "none"});
        jQuery("body").css({"overflow-y": "scroll"});
    });

    jQuery(".cf-header i").click(function() {
        jQuery(".custom-modal").css({"display": "none"});
        jQuery("body").css({"overflow-y": "scroll"});
    });
    
    // jQuery(".custom-modal-close-new").click(function() {
    //     jQuery(".custom-modal").css({"display": "none"});
    //     jQuery("body").css({"overflow-y": "scroll"});
    // });

    jQuery('body').on('click', '.custom-modal-close-new', function(){
        jQuery(".custom-modal").css({"display": "none"});
        jQuery("body").css({"overflow-y": "scroll"});
    });

   // jQuery(".prod-cat-section .card-carousel-buttons").height(jQuery('#prod_cat_slider .prod-cat-card').height());


    //
    // cross-sell-row  col

    if(jQuery('.cross-sell-row-part1').length)
    {
        var maxHeight = 0;
        jQuery('.cross-sell-row-part1').find( ".cs-item" ).each(function( index ) {

            console.log(jQuery(this ).height());
            if( jQuery(this ).height() > maxHeight )
            {
                maxHeight = jQuery(this ).height();
            }
        });

        jQuery('.cross-sell-row-part1').find( ".cs-item" ).each(function( index ) {
            jQuery(this).height( maxHeight );
        });
    }
     if(jQuery('.cross-sell-row-part2').length)
    {
        var maxHeight = 0;
        jQuery('.cross-sell-row-part2').find( ".cs-item" ).each(function( index ) {

            console.log(jQuery(this ).height());
            if( jQuery(this ).height() > maxHeight )
            {
                maxHeight = jQuery(this ).height();
            }
        });

        jQuery('.cross-sell-row-part2').find( ".cs-item" ).each(function( index ) {
            jQuery(this).height( maxHeight );
        });
    }

    jQuery(window).on('resize', function()
    {
        orientation_change();
    });

    // Phase 2

    //jQuery( '<div class="nav-overlay"></div>' ).insertAfter( ".header-wrapper" );
    
    // jQuery( ".dropdownDtl" ).mouseenter(function() {
    //     jQuery('.nav-overlay').css('display','block');
    //     console.log(1);
    // });
    // jQuery( ".main-nav" ).mouseenter(function() {
    //     jQuery('.nav-overlay').css('display','none');
    //     console.log(0);
    // });
    // jQuery( ".nav-overlay" ).mouseenter(function() {
    //     jQuery('.nav-overlay').css('display','none');
    //     console.log(-1);
    // });

    jQuery( ".dropdownDtl" )
    .mouseenter(function() {
        jQuery('.nav-overlay').css('display','block');

        jQuery('.page-wrapper').css('overflow-y','hidden');

        console.log(1);
    })
    .mouseleave(function() {
        jQuery('.nav-overlay').css('display','none');

        jQuery('.page-wrapper').css('overflow-y','auto');
        console.log(0);
    });

      

    jQuery(".footer-row .col h5").click(function() {

        // jQuery(".footer-row .col").removeClass('active');
        // jQuery(this).addClass('active');
       
        jQuery(this).parent().toggleClass('active');
       
    });
	jQuery('body').on('click', '.filter-trigger', function(){
        var itemTop = jQuery( this ).offset();
        //jQuery('.sidebar.col-sm-2').css("top",itemTop.top.toString()+"px !important");
        jQuery('.sidebar.col-sm-2').attr("style", "display: block; top: "+itemTop.top+"px !important;");
        mobile_filter_show()
    });
    // jQuery(".filter-trigger").click(function() {

        // var itemTop = jQuery( this ).offset();
        // jQuery('.sidebar.col-sm-2').css('top',itemTop.top+'px');
        // mobile_filter_show()
    // });
    // jQuery(".mobilelistshowfilter").click(function() {
        // mobile_filter_show()
    // });
	jQuery('body').on('click', '.mobilelistshowfilter', function(){
        mobile_filter_show();
    });
    jQuery(".filter-cancel").click(function() {
        mobile_filter_hide()
    });
    jQuery(".filter-apply").click(function() {
        mobile_filter_hide()
    });
    jQuery(".mobilelistshowfilter").click(function() {
        //mobile_filter_show()
        //action primary clear-button
    });
    
    // jQuery('.prod_cat_slider').slick({
    //     centerMode: true,
    //     infinite: true,
    //     variableWidth: false

    // });
    //
    

    jQuery('body').on('click', '.pagination a', function(){
        jQuery("html, body").animate({ scrollTop: jQuery("#maincontent").offset().top }, "fast");
    });



 
   // 

    jQuery( ".sidenav-trigger" ).click(function() {
       jQuery('.close-btn').css('display','flex');
       jQuery('.close-btn').css('opacity','1');
       jQuery('.main-nav>.container').css('pointer-events','none');
    });

    jQuery( ".sidenav-overlay" ).click(function() {
        jQuery('.close-btn').css('opacity','1');
        jQuery('.close-btn').css('display','none');
        jQuery('.main-nav>.container').css('pointer-events','all');
     });
     jQuery( ".close-btn" ).click(function() {
        // jQuery('.close-btn').css('opacity','1');
        // jQuery('.close-btn').css('display','none');
        jQuery(".sidenav-overlay").trigger('click');
     });
    
     //
    //  jQuery( ".clear-filters" ).click(function() {
    //     jQuery(".button-list .action.primary.clear-button").trigger('click');
    //  });
    jQuery('body').on('click', '.clear-filters', function(){
        console.log(12);
        jQuery(".button-list .action.primary.clear-button").trigger('click');
    });

    // filter for mobile action
    //
    if(jQuery(window).width() <= 768) 
    {
        // for mobile first time only
        jQuery(".iformobiletitle").removeClass('active');
        jQuery(".iformobile.filter-options-item").removeClass('active');
        jQuery(".iformobile.filter-options-item").find('.filter-options-content').css('display','none');

        if(localStorage.getItem("dtlLastMobileDisplay")!="")
        {
            show_product_to(localStorage.getItem("dtlLastMobileDisplay"));
        }
		

    }   
    move_filter_accordingly();

    // jQuery('.second').click(function() {
    //     jQuery(".sort-popup").toggleClass('popup-show');
    //     var position = jQuery(this).position();
    //     console.log(position.top);
    //     jQuery(".sort-popup").css('top',position.top+'px');

    //  });
    //  jQuery('.third').click(function() {
    //     jQuery(".view-popup").toggleClass('popup-show');
    //     var position = jQuery(this).position();
    //     console.log(position.top);
    //     jQuery(".view-popup").css('top',position.top+'px');

    //  });

    // this is for mibile title click
    render_mobile_filter_click();
    fc_slider();
    
    // 

    jQuery('.listing-header .show-more-btn').click(function() {
        jQuery(this).parent().toggleClass('active');
    });
});
function fc_slider()
{
    var itemWidth = jQuery('.featured-collection-item').width()+12;
    var maxScroll = jQuery('.featured-collection-item').length - 2;

    // Next
    jQuery('#featured_collection .next-btn').click(function(){
        ///console.log( jQuery('.featured-collection-item').css( "transform" ) );
        var currentTransform = getXval( jQuery('.featured-collection-item') );

        currentTransform = Math.abs(currentTransform);
        ///console.log(currentTransform);
        if(currentTransform>0)
        {
            var newVal = parseInt(currentTransform) + parseInt(itemWidth);
            if(newVal < (maxScroll * itemWidth) )
            {
            jQuery('.featured-collection-item').css('transform','translateX(-'+newVal+'px)');
            }
        }
        else
        {
            jQuery('.featured-collection-item').css('transform','translateX(-'+itemWidth+'px)');
        }
    });
    // Prev
    jQuery('#featured_collection .prev-btn').click(function(){
        ///console.log( jQuery('.featured-collection-item').css( "transform" ) );
        var currentTransform = getXval( jQuery('.featured-collection-item') );

        currentTransform = Math.abs(currentTransform);
        ///console.log(currentTransform);
        if(currentTransform>0)
        {
            var newVal = parseInt(currentTransform) - parseInt(itemWidth);
            if(newVal < (maxScroll * itemWidth) )
            {
            jQuery('.featured-collection-item').css('transform','translateX(-'+newVal+'px)');
            }
        }
        else
        {
            jQuery('.featured-collection-item').css('transform','translateX(-0px)');
        }
    });
}
function getXval(obj)
{
    var current_pull = parseInt(obj.css('transform').split(',')[4]);
    ///console.log(current_pull)
    return current_pull;
}
function sort_me(mainA,mainB)
{
    jQuery('#sorter').val(mainA);
    jQuery('#sorter').trigger('change')
}
function show_product_to(setOpt)
{
    if(setOpt==1)
    {
        jQuery(".products.products-grid").removeClass('list-two-col'); 
        jQuery(".products.products-grid").addClass('list-single-col'); 
        localStorage.setItem("dtlLastMobileDisplay", 1);
    }
    if(setOpt==2)
    {
        jQuery(".products.products-grid").addClass('list-two-col'); 
        jQuery(".products.products-grid").removeClass('list-single-col'); 
        localStorage.setItem("dtlLastMobileDisplay", 2);
    }
    
}
function render_mobile_filter_click()
{
    jQuery(".tablist").find('.iformobiletitle').click(function() {
        
        var iamID = jQuery(this).attr('id');
        console.log(iamID);
        // Mibile Title Display 
        if(!jQuery(this).hasClass('active'))
        {
            jQuery(".iformobile#"+iamID).find('.filter-options-title').trigger('click');
        }
        jQuery(".iformobiletitle").removeClass('active');
        jQuery(this).addClass('active');
    });
    //
    jQuery(".mobilelistshowfilter").click(function() {
        mobile_filter_show()
    });
    jQuery(".filter-cancel").click(function() {
        mobile_filter_hide()
    });
    jQuery(".filter-apply").click(function() {
        mobile_filter_hide()
    });
    jQuery( ".clear-filters" ).click(function() {
        jQuery(".button-list .action.primary.clear-button").trigger('click');
     });
    //
    jQuery('.second').click(function() {
        jQuery(".sort-popup").toggleClass('popup-show');
        jQuery(".view-popup").removeClass('popup-show');
        var position = jQuery(this).position();
        console.log(position.top);
        jQuery(".sort-popup").css('top',position.top+'px');

     });
     jQuery('.third').click(function() {
        jQuery(".view-popup").toggleClass('popup-show');
        jQuery(".sort-popup").removeClass('popup-show');
        var position = jQuery(this).position();
        console.log(position.top);
        jQuery(".view-popup").css('top',position.top+'px');

     });
}
function delay_call()
{
    jQuery(".iformobiletitle").removeClass('active');
    jQuery(".iformobile.filter-options-item").removeClass('active');
    jQuery(".iformobile.filter-options-item").find('.filter-options-content').css('display','none');

    if( localStorage.getItem("dtlLastClicked") !== null && localStorage.getItem("dtlLastClicked")!="")
    {
        console.log('set item:'+localStorage.getItem("dtlLastClicked"));
        jQuery(".iformobiletitle#"+localStorage.getItem("dtlLastClicked")).trigger('click');
    }
    else
    {
        console.log('set default');
        jQuery(".iformobiletitle").first().trigger('click');
    }
    if( localStorage.getItem("dtlLastMobileDisplay")  !== null && localStorage.getItem("dtlLastMobileDisplay")!="")
    {
        show_product_to(localStorage.getItem("dtlLastMobileDisplay"));
    }
    move_filter_accordingly();
}
function move_filter_accordingly()
{
    if(jQuery(window).width() <= 768) 
    {
        
        //filter-options-item
         if(jQuery('#layered-selected-filters-block').length)
        {
            jQuery( "#aw-layered-nav-popup" ).after( jQuery( "#layered-selected-filters-block" ) );
        }
    }
    else
    {
         if(jQuery('#layered-selected-filters-block').length)
        {
            jQuery( ".toolbar-products" ).first().after( jQuery( "#layered-selected-filters-block" ) );
        }
    }
}


function mobile_filter_show()
{
    jQuery(".sort-popup").removeClass('popup-show');
    jQuery(".view-popup").removeClass('popup-show');
    filter_set_mobile();
    jQuery('.sidebar.col-sm-2').css('display','block');
    // jQuery('body').css('overflow-y','hidden');
    // jQuery('body').css('-webkit-overflow-scrolling','touch');
    wHeight = window.innerHeight;
    //jQuery('.block.filter').height(wHeight);
    //jQuery('.block-content.filter-content').height(wHeight-184);

    jQuery('.page-wrapper').addClass('body-no-scroll');
    
    
    delay_call();
}
function mobile_filter_hide()
{
    jQuery('.sidebar.col-sm-2').css('display','none');
    // jQuery('body').css('overflow-y','scroll');
    jQuery('.page-wrapper').removeClass('body-no-scroll');
}
function filter_set_mobile()
{
    // if(localStorage.getItem("lastname")!="")
    // {

    // }
    // else
    // {

    // }
}
function mobile_filter_check_and_run()
{

   // delay_call();
}
function mobile_filter_check_and_run_main()
{
    if(jQuery(window).width() <= 768) 
    {
        
        jQuery(".mobilelistshowfilter").click(function() {
            mobile_filter_show()
        });
        jQuery(".filter-cancel").click(function() {
            mobile_filter_hide()
        });
        jQuery(".filter-apply").click(function() {
            mobile_filter_hide()
        });
        filter_set_mobile();
        move_filter_accordingly();
    }
    
    jQuery(".tablist").find('.iformobiletitle').click(function() {
        
        var iamID = jQuery(this).attr('id');
        console.log(iamID);
        // Mibile Title Display 
        jQuery(".iformobiletitle").removeClass('active');
        jQuery(this).addClass('active');
        // Filter Deisplay
        jQuery(".iformobile.filter-options-item").removeClass('active');
        jQuery(".iformobile.filter-options-item").find('.filter-options-content').css('display','none');

        jQuery(".iformobile#"+iamID).find('.filter-options-title').trigger('click');
    });
}
function filter_set_mobile_main()
{

    // Filter title disply 
    if( jQuery(".iformobiletitle.filter-options-item.active").length < 1 )
    {
        jQuery(".iformobiletitle.filter-options-item").first().addClass('active');
    }
    else
    {
        var getActive = false;
        jQuery(".iformobiletitle.filter-options-item").each(function () {

            if(getActive===true )
            {
                jQuery(this).removeClass('active') ;
            }
            else
            {
                if(jQuery(this).hasClass('active') )
                {
                    getActive = true;
                }
            }
        });
    }
    // Filter Option Display
    if( jQuery(".iformobile.filter-options-item.active").length < 1 )
    {
        jQuery(".iformobile.filter-options-item").first().addClass('active');
        jQuery(".iformobile.filter-options-item.active").find('.filter-options-content').css('display','block');
    }
    else
    {
        var getActive = false;
        jQuery(".iformobile.filter-options-item").each(function () {

            if(getActive===true )
            {
                jQuery(this).removeClass('active') ;
                jQuery(this).find(".filter-options-content").css('display','none');
            }
            else
            {
                if(jQuery(this).hasClass('active') )
                {
                    getActive = true;
                }
            }
        });
    }
}
function orientation_change()
{
    if (jQuery(window).width() < 420) {
        jQuery('.search-bar').css({"display": "flex"});
        jQuery('.search-bar-desktop').css({"display": "none"});
        var searchMobileWidth = jQuery('.search-mobile').width();
        var positionLeft = jQuery('.search-mobile').offset().left;
        var positionTop = jQuery('.search-mobile').offset().top;

        jQuery( ".search-mobile").change(function() {
            jQuery( ".search-popup").css({"display": "block"});
            jQuery('.add-to-bag-btn').css({"display": "none"});
            jQuery('.buy-now-btn').css({"display": "none"});

            jQuery(".nav-search").val(jQuery( ".search-mobile").val());
        });


        jQuery( "main").click(function() {
            jQuery( ".search-popup").css({"display": "none"});
            jQuery('.add-to-bag-btn').css({"display": "block"});
            jQuery('.buy-now-btn').css({"display": "block"});
        });
 
    }
    else 
    {
        
        jQuery('.search-bar').css({"display": "none"});
        jQuery('.search-bar-desktop').css({"display": "block"});

        if(jQuery('.nav-search').length)
        {
            var navSearchWidth = jQuery('.nav-search').width();
            var positionLeft = jQuery('.nav-search').offset().left;
            jQuery('.search-popup').width(navSearchWidth+13);
            jQuery('.search-popup').css({"left": 12 });
            jQuery('.search-popup').css({"top": 41 });
        }

        jQuery(".nav-search").change(function() {
            jQuery( ".search-popup").css({"display": "block"});
        });

        jQuery("main").click(function() {
            jQuery( ".search-popup").css({"display": "none"});
        });
    }

    



}
// var allowToAdd = false ;  
