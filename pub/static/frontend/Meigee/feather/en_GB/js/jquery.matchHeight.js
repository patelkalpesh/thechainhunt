/**
* jquery-match-height master by @liabru
* http://brm.io/jquery-match-height/
* License: MIT
*/

;(function(factory) { // eslint-disable-line no-extra-semi
    'use strict';
     factory(jQuery);
})(function(jQuery) {
    /*
    *  internal
    */

    var _previousResizeWidth = -1,
        _updateTimeout = -1;

    /*
    *  _parse
    *  value parse utility function
    */

    var _parse = function(value) {
        // parse value and convert NaN to 0
        return parseFloat(value) || 0;
    };

    /*
    *  _rows
    *  utility function returns array of jQuery selections representing each row
    *  (as displayed after float wrapping applied by browser)
    */

    var _rows = function(elements) {
        var tolerance = 1,
            JQelements = jQuery(elements),
            lastTop = null,
            rows = [];

        // group elements by their top position
        JQelements.each(function(){
            var JQthat = jQuery(this),
                top = JQthat.offset().top - _parse(JQthat.css('margin-top')),
                lastRow = rows.length > 0 ? rows[rows.length - 1] : null;

            if (lastRow === null) {
                // first item on the row, so just push it
                rows.push(JQthat);
            } else {
                // if the row top is the same, add to the row group
                if (Math.floor(Math.abs(lastTop - top)) <= tolerance) {
                    rows[rows.length - 1] = lastRow.add(JQthat);
                } else {
                    // otherwise start a new row group
                    rows.push(JQthat);
                }
            }

            // keep track of the last row top
            lastTop = top;
        });

        return rows;
    };

    /*
    *  _parseOptions
    *  handle plugin options
    */

    var _parseOptions = function(options) {
        var opts = {
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        };

        if (typeof options === 'object') {
            return jQuery.extend(opts, options);
        }

        if (typeof options === 'boolean') {
            opts.byRow = options;
        } else if (options === 'remove') {
            opts.remove = true;
        }

        return opts;
    };

    /*
    *  matchHeight
    *  plugin definition
    */

    var matchHeight = jQuery.fn.matchHeight = function(options) {
        var opts = _parseOptions(options);

        // handle remove
        if (opts.remove) {
            var that = this;

            // remove fixed height from all selected elements
            this.css(opts.property, '');

            // remove selected elements from all groups
            jQuery.each(matchHeight._groups, function(key, group) {
                group.elements = group.elements.not(that);
            });

            // TODO: cleanup empty groups

            return this;
        }

        if (this.length <= 1 && !opts.target) {
            return this;
        }

        // keep track of this group so we can re-apply later on load and resize events
        matchHeight._groups.push({
            elements: this,
            options: opts
        });

        // match each element's height to the tallest element in the selection
        matchHeight._apply(this, opts);

        return this;
    };

    /*
    *  plugin global options
    */

    matchHeight.version = 'master';
    matchHeight._groups = [];
    matchHeight._throttle = 80;
    matchHeight._maintainScroll = false;
    matchHeight._beforeUpdate = null;
    matchHeight._afterUpdate = null;
    matchHeight._rows = _rows;
    matchHeight._parse = _parse;
    matchHeight._parseOptions = _parseOptions;

    /*
    *  matchHeight._apply
    *  apply matchHeight to given elements
    */

    matchHeight._apply = function(elements, options) {
        var opts = _parseOptions(options),
            JQelements = jQuery(elements),
            rows = [JQelements];

        // take note of scroll position
        var scrollTop = jQuery(window).scrollTop(),
            htmlHeight = jQuery('html').outerHeight(true);

        // get hidden parents
        var JQhiddenParents = JQelements.parents().filter(':hidden');

        // cache the original inline style
        JQhiddenParents.each(function() {
            var JQthat = jQuery(this);
            JQthat.data('style-cache', JQthat.attr('style'));
        });

        // temporarily must force hidden parents visible
        JQhiddenParents.css('display', 'block');

        // get rows if using byRow, otherwise assume one row
        if (opts.byRow && !opts.target) {

            // must first force an arbitrary equal height so floating elements break evenly
            JQelements.each(function() {
                var JQthat = jQuery(this),
                    display = JQthat.css('display');

                // temporarily force a usable display value
                if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                    display = 'block';
                }

                // cache the original inline style
                JQthat.data('style-cache', JQthat.attr('style'));

                JQthat.css({
                    'display': display,
                    'padding-top': '0',
                    'padding-bottom': '0',
                    'margin-top': '0',
                    'margin-bottom': '0',
                    'border-top-width': '0',
                    'border-bottom-width': '0',
                    'height': '100px',
                    'overflow': 'hidden'
                });
            });

            // get the array of rows (based on element top position)
            rows = _rows(JQelements);

            // revert original inline styles
            JQelements.each(function() {
                var JQthat = jQuery(this);
                JQthat.attr('style', JQthat.data('style-cache') || '');
            });
        }

        jQuery.each(rows, function(key, row) {
            var JQrow = jQuery(row),
                targetHeight = 0;

            if (!opts.target) {
                // skip apply to rows with only one item
                if (opts.byRow && JQrow.length <= 1) {
                    JQrow.css(opts.property, '');
                    return;
                }

                // iterate the row and find the max height
                JQrow.each(function(){
                    var JQthat = jQuery(this),
                        style = JQthat.attr('style'),
                        display = JQthat.css('display');

                    // temporarily force a usable display value
                    if (display !== 'inline-block' && display !== 'flex' && display !== 'inline-flex') {
                        display = 'block';
                    }

                    // ensure we get the correct actual height (and not a previously set height value)
                    var css = { 'display': display };
                    css[opts.property] = '';
                    JQthat.css(css);

                    // find the max height (including padding, but not margin)
                    if (JQthat.outerHeight(false) > targetHeight) {
                        targetHeight = JQthat.outerHeight(false);
                    }

                    // revert styles
                    if (style) {
                        JQthat.attr('style', style);
                    } else {
                        JQthat.css('display', '');
                    }
                });
            } else {
                // if target set, use the height of the target element
                targetHeight = opts.target.outerHeight(false);
            }

            // iterate the row and apply the height to all elements
            JQrow.each(function(){
                var JQthat = jQuery(this),
                    verticalPadding = 0;

                // don't apply to a target
                if (opts.target && JQthat.is(opts.target)) {
                    return;
                }

                // handle padding and border correctly (required when not using border-box)
                if (JQthat.css('box-sizing') !== 'border-box') {
                    verticalPadding += _parse(JQthat.css('border-top-width')) + _parse(JQthat.css('border-bottom-width'));
                    verticalPadding += _parse(JQthat.css('padding-top')) + _parse(JQthat.css('padding-bottom'));
                }

                // set the height (accounting for padding and border)
                JQthat.css(opts.property, (targetHeight - verticalPadding) + 'px');
            });
        });

        // revert hidden parents
        JQhiddenParents.each(function() {
            var JQthat = jQuery(this);
            JQthat.attr('style', JQthat.data('style-cache') || null);
        });

        // restore scroll position if enabled
        if (matchHeight._maintainScroll) {
            jQuery(window).scrollTop((scrollTop / htmlHeight) * jQuery('html').outerHeight(true));
        }

        return this;
    };

    /*
    *  matchHeight._applyDataApi
    *  applies matchHeight to all elements with a data-match-height attribute
    */

    matchHeight._applyDataApi = function() {
        var groups = {};

        // generate groups by their groupId set by elements using data-match-height
        jQuery('[data-match-height], [data-mh]').each(function() {
            var JQthis = jQuery(this),
                groupId = JQthis.attr('data-mh') || JQthis.attr('data-match-height');

            if (groupId in groups) {
                groups[groupId] = groups[groupId].add(JQthis);
            } else {
                groups[groupId] = JQthis;
            }
        });

        // apply matchHeight to each group
        jQuery.each(groups, function() {
            this.matchHeight(true);
        });
    };

    /*
    *  matchHeight._update
    *  updates matchHeight on all current groups with their correct options
    */

    var _update = function(event) {
        if (matchHeight._beforeUpdate) {
            matchHeight._beforeUpdate(event, matchHeight._groups);
        }

        jQuery.each(matchHeight._groups, function() {
            matchHeight._apply(this.elements, this.options);
        });

        if (matchHeight._afterUpdate) {
            matchHeight._afterUpdate(event, matchHeight._groups);
        }
    };

    matchHeight._update = function(throttle, event) {
        // prevent update if fired from a resize event
        // where the viewport width hasn't actually changed
        // fixes an event looping bug in IE8
        if (event && event.type === 'resize') {
            var windowWidth = jQuery(window).width();
            if (windowWidth === _previousResizeWidth) {
                return;
            }
            _previousResizeWidth = windowWidth;
        }

        // throttle updates
        if (!throttle) {
            _update(event);
        } else if (_updateTimeout === -1) {
            _updateTimeout = setTimeout(function() {
                _update(event);
                _updateTimeout = -1;
            }, matchHeight._throttle);
        }
    };

    /*
    *  bind events
    */

    // apply on DOM ready event
    jQuery(matchHeight._applyDataApi);

    // use on or bind where supported
    var on = jQuery.fn.on ? 'on' : 'bind';

    // update heights on load and resize events
    jQuery(window)[on]('load', function(event) {
        matchHeight._update(false, event);
    });

    // throttled update heights on resize events
    jQuery(window)[on]('resize orientationchange', function(event) {
        matchHeight._update(true, event);
    });

});