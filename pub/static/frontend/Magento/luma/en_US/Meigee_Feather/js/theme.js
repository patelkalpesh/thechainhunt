function customHomeSlider() {
	slider = jQuery("#home-slider");
	navigation = slider.data('navigation');
	pagination = slider.data('pagination');
	slideSpeed = slider.data('speed');
	navigation ? navigation = true : navigation = false;
	pagination ? pagination = true : pagination = false;
	slider.owlCarousel({
		items : 1,
		navigation : navigation,
		slideSpeed : slideSpeed,
		pagination: pagination,
		paginationSpeed : 400,
		singleItem: true
	});
}

function pageNotFound() {
	if(jQuery('.not-found-bg').data('bgimg')){
		var bgImg = jQuery('.not-found-bg').data('bgimg');
		jQuery('.not-found-bg').attr('style', bgImg);
	}
}

function accordionNav(){

	/*
	if(jQuery('.block.filter').length){
		jQuery('.filter-options-title').off().on('click', function(){
			jQuery(this).parents('.filter-options-item').toggleClass('active').children('.filter-options-content').slideToggle();
			
			//jQuery(this).toggleClass('active').next('.filter-options-content').slideToggle();
		});
		if(jQuery(document.body).width() < 767 && jQuery('body').hasClass('page-layout-1column')){
			jQuery('#layered-filter-block .filter-title').on('click', function(){
				if(!jQuery('#layered-filter-block').hasClass('active')) {
					jQuery('#layered-filter-block').addClass('active');
				} else {
					jQuery('#layered-filter-block').removeClass('active');
				}
			});

		}
	}
	*/
}

function headerSearch(){
	if (jQuery('.block-search').hasClass('type-2')) {
		jQuery('body .page-wrapper').append('<div id="search-inner"></div>')
	};
	jQuery('header .search-button').each(function(){
		jQuery(this).off().on('click', function(){
			if(jQuery(this).parents('.block-search').hasClass('type-2') || jQuery(this).parents('.block-search').hasClass('type-3')){
				jQuery('body').addClass('search-open');
				jQuery(this).parents('.block-search')
					.addClass('active')
					.find('.indent').append('<span class="btn-close" />').css({'left': 0}).animate({
						'opacity' : 1
					}, 150);
			}
			if(jQuery(this).parents('.block-search').hasClass('active') && jQuery(this).parents('.block-search').find('.btn-close').length){
				jQuery(this).parents('.block-search').find('.btn-close').on('click', function(){
					jQuery('body').removeClass('search-open');
					jQuery(this).parents('.indent').css('left', '-100%').animate({
						'opacity' : 0,
						'z-index' : '-1'
					}, 150);
					jQuery(this).parents('.block-search')
						.removeClass('active')
						.find('.btn-close').remove();
				});
				jQuery(document).on('click.searchEvent', function(e){
					if(jQuery(e.target).parents('.block-search.type-2').length == 0){
						jQuery('body').removeClass('search-open');
						jQuery('.block-search.type-2').find('.indent').css('left', '-100%').animate({
							'opacity' : 0,
							'z-index' : '-1'
						}, 150);
						jQuery('.block-search')
						.removeClass('active')
						.find('.btn-close').remove();
						jQuery(document).off('click.searchEvent');
					}
				});
				if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i))){
					jQuery(document).on('touchstart', function(e){
						if(jQuery(e.target).parents('.block-search.type-2').length == 0){
							jQuery('body').removeClass('search-open');
							jQuery('.form-search.type-1').find('.indent').css('left', '-100%').animate({
								'opacity' : 0,
								'z-index' : '-1'
							}, 150);
							jQuery('.form-search')
							.removeClass('active')
							.find('.btn-close').remove();
							jQuery(document).off('touchstart');
						}
					});
				}

			}
		});
	});
}


function custom_top_menu_button(){
	if(jQuery('header').hasClass('header-7')){
            navButton = jQuery('header .action.nav-toggle').clone();
            jQuery('header .action.nav-toggle').after(navButton);
            jQuery(navButton).addClass('custom-menu-button');
	}
}
function custom_top_menu_button_listener(){
	if (window.innerWidth > 1007){
		jQuery('header .custom-menu-button').show();
		jQuery('div.topmenu.custom-top-menu').show();
	} else {
	    jQuery('.mobile-menu-wrapper div.topmenu').removeClass('custom-top-menu');
		jQuery('header .custom-menu-button').hide();
		jQuery('div.topmenu.custom-top-menu').hide();
		jQuery('.mobile-menu-wrapper div.topmenu ul li').attr('style', '');
		jQuery('.mobile-menu-wrapper div.topmenu ul li a').attr('style', '');
	}
}

function custom_top_menu(mode){
	jQuery('header.page-header').each(function(){
	    switch(mode)
	     {
	     case 'animate':
	       if(!jQuery('div.topmenu').hasClass('custom-top-menu')){
				var isMenuAnimation = false;
	            jQuery("div.topmenu").addClass('custom-top-menu');
	            jQuery(".mobile-menu-wrapper div.topmenu").removeClass('custom-top-menu');
		        menuButton = jQuery('.custom-menu-button');
	            if(menuButton.length){
	                menuButton.removeClass('active').children('.custom-menu-button').removeClass('close');
	                var isActiveMenu = false;
	                var isTouch = false;
	                if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i))){
	                    var isTouch = true;
	                }
	                function callEvent(event){
	                    event.stopPropagation();
	                    if(isActiveMenu == false && !isMenuAnimation){
	                        isMenuAnimation = true;
	                        menuButton.addClass('active').children('.custom-menu-button').addClass('close');
	                        jQuery(this).addClass('active');
	                        menuWidth = jQuery('header .topmenu.navbar-collapse').css('width');
	                        jQuery('header div.topmenu.custom-top-menu').addClass('in');
							jQuery('header ul.topmenu').css('visibility', 'visible');
	                        jQuery('header ul.topmenu li.level0').css('height', 'auto');
	                        jQuery(jQuery('header div.topmenu.custom-top-menu > ul > li.level0 > a').get().reverse()).each(function(i) {
								i ++;
								jQuery(this).addClass('animation-progress').animate({'top' : '0' , 'opacity' : '1'}, i*50, function(){
									jQuery(this).removeClass('animation-progress');
									isMenuAnimation = false;
								});
							});

	                        isActiveMenu = true;
	                        if(isTouch){
	                            document.addEventListener('touchstart', mobMenuListener, false);
	                        }else{
	                            jQuery(document).on('click.mobMenuEvent', function(e){
	                                if(jQuery(e.target).parents('header div.topmenu.custom-top-menu').length == 0){
	                                    closeMenu();
	                                    document.removeEventListener('touchstart', mobMenuListener, false);
	                                    jQuery(document).off('click.mobMenuEvent');
	                                }
	                            });
	                        }
	                    }else if(!isMenuAnimation){
	                        closeMenu();
	                    }
	                }

	                if(!isTouch){
	                    menuButton.on('click.menu', function(event){
	                        callEvent(event);
	                    });
	                }else{
	                    document.getElementsByClassName('custom-menu-button')[0].addEventListener('touchstart', callEvent, false);
	                }

	                function closeMenu(eventSet){
	                    menuButton.removeClass('active').children('.custom-menu-button').removeClass('close');
	                    isMenuAnimation = true;
	                    jQuery('header div.topmenu.custom-top-menu').removeClass('in');
	                    var count = jQuery("header div.topmenu.custom-top-menu > ul > .level0").size();
						jQuery(jQuery('header div.topmenu.custom-top-menu > ul > li.level0 > a').get().reverse()).each(function(i) {
							i ++;
							jQuery(this).addClass('animation-progress').animate({'top' : '-50px' , 'opacity' : '0'}, i*50, function(){
								jQuery(this).removeClass('animation-progress');
							});
	                        isMenuAnimation = false;
	                        isActiveMenu = false;
							if(i === count) {
								function hideMenu (){
									jQuery('header div.topmenu.custom-top-menu > ul > li.level0').each(function() {
										jQuery(this).css('height', 0);
									});
								}
								setTimeout(hideMenu, i*30);
							}
						});

	                    document.removeEventListener('touchstart', mobMenuListener, false);
	                }
	                function mobMenuListener(e){
	                    var touch = e.touches[0];
	                    if(jQuery(touch.target).parents('div.topmenu.custom-top-menu').length == 0 && jQuery(touch.target).parents('.custom-menu-button').length == 0 && !jQuery(touch.target).hasClass('custom-menu-button')){
	                        closeMenu();
	                    }
	                }
	            }
	       }
	       break;
	       default:
	       jQuery('.mobile-menu-wrapper div.topmenu').removeClass('custom-top-menu');
	       jQuery('div.topmenu ul').attr('style', '');
	       jQuery('div.topmenu ul li').attr('style', '');
	       jQuery('div.topmenu ul li a').attr('style', '');
	       jQuery('.custom-menu-button').off();
	       jQuery('.lines-button').on('click', function(){
	       	jQuery(this).toggleClass('close');
	       	jQuery('.menu-block').toggleClass('open');
	       	if(!jQuery('.menu-block').hasClass('open')){
	       		setTimeout(function(){
	       			jQuery('.menu-block').attr('style', '').find('row').css('width', '1200px');
	       		}, 500);
	       	} else {
	       		setTimeout(function(){
	       			jQuery('.menu-block').css('overflow', 'visible').find('row').css('width', 'auto');
	       		}, 500);
	       	}
	       });
	   }
	});
}
function replacingClass () {

	if (window.innerWidth < 480) { //Mobile
		if (jQuery('header').hasClass('header-7')) {
			custom_top_menu('animate');
		}
	}
	if (window.innerWidth > 479 && window.innerWidth < 768) { //iPhone
		if (jQuery('header').hasClass('header-7')) {
			custom_top_menu('animate');
		}
	}
	if (window.innerWidth > 767 && window.innerWidth <= 1007){ //Tablet
		if (jQuery('header').hasClass('header-7')) {
			custom_top_menu('animate');
		}
	}
	if (window.innerWidth > 1007 && window.innerWidth <= 1374){ //Desktop
		if (jQuery('header').hasClass('header-7')) {
			custom_top_menu('animate');
		}
	}
	if (window.innerWidth > 1374){ //Extra Large
		if (jQuery('header').hasClass('header-7')) {
			custom_top_menu('animate');
		}
	}

}

/* Header Customer Block */
function headerCustomer() {
	if(jQuery('header.page-header .customer-block').length){
		jQuery('.customer-block').prepend('<div class="customer-name-wrapper"><span class="customer-name"><span class="user-icon"><i class="fa fa-navicon"></i></span></span></div>') ;
		var custName = jQuery('header.page-header .customer-name-wrapper');
		jQuery('header.page-header .links').hide();
		jQuery('header.page-header .customer-name').removeClass('open');
		jQuery('header.page-header .customer-name + .links').slideUp(500);
		jQuery('header.page-header .links li').each(function(){
			jQuery(this).find('a').prepend('<i class="fa fa-circle" />').append('<span class="hover-divider" />');
		});
		function headerCustomerListener(e){
			console.log('0');
			var touch = e.touches[0];
			if(jQuery(touch.target).parents('header.page-header .customer-name + .links').length == 0 && !jQuery(touch.target).hasClass('customer-name') && !jQuery(touch.target).parents('.customer-name').length){
				jQuery('header.page-header .customer-name').removeClass('open');
				jQuery('header.page-header .customer-name + .links').slideUp(500);
				document.removeEventListener('touchstart', headerCustomerListener, false);
			}
		}
		custName.parent().off().on('mouseenter', function(event){
			event.stopPropagation();
			jQuery(this).children().addClass('hover');
		});
		custName.parent().on('mouseleave', function(event){
			console.log('1');
			event.stopPropagation();
			jQuery(this).children().removeClass('hover');
		});
		custName.off().on('click', function(event){
			console.log('2');
			event.stopPropagation();
			jQuery(this).toggleClass('open');
			var linksTop = custName.position().top + custName.outerHeight(true);
			jQuery('header.page-header .links').slideToggle().css('top', linksTop);
			document.addEventListener('touchstart', headerCustomerListener, false);
			jQuery(document).on('click.headerCustomerEvent', function(e) {
				if (jQuery(e.target).parents('header.page-header ul.links').length == 0) {
					jQuery('header.page-header .customer-name').removeClass('open');
					jQuery('header.page-header .customer-name + .links').slideUp(500);
					jQuery(document).off('click.headerCustomerEvent');
				}
			});
		});
	}
}

function backgroundWrapper(){
	if(jQuery('.background-wrapper').length){
		jQuery('.background-wrapper').each(function(){
			var thisBg = jQuery(this);
			if(jQuery(document.body).width() < 768){
				thisBg.attr('style', '');
				if(thisBg.parent().hasClass('text-banner') || thisBg.find('.text-banner').length || thisBg.find('.fullwidth-text-banners').length){
					bgHeight = thisBg.parent().outerHeight();
					thisBg.parent().css('height', bgHeight - 2);
				}
				if(jQuery('body').hasClass('boxed-layout')){
					bodyWidth = thisBg.parents('.container').outerWidth();
					bgLeft = (bodyWidth - thisBg.parents('.container').width())/2;
				} else {
					bgLeft = thisBg.parent().offset().left;
					bodyWidth = jQuery(document.body).width();
				}
				if(thisBg.data('bgColor')){
					bgColor = thisBg.data('bgColor');
					thisBg.css('background-color', bgColor);
				}
				setTimeout(function(){
					thisBg.css({
						'position' : 'absolute',
						'left' : -bgLeft,
						'width' : bodyWidth
					}).parent().css('position', 'relative');
				}, 300);
			} else {
				thisBg.attr('style', '');
				if(jQuery('body').hasClass('boxed-layout')){
					bodyWidth = thisBg.parents('.container').outerWidth();
					bgLeft = (bodyWidth - thisBg.parents('.container').width())/2;
				} else {
					bgLeft = thisBg.parent().offset().left;
					bodyWidth = jQuery(document.body).width();
				}
				thisBg.css({
					'position' : 'absolute',
					'left' : -bgLeft,
					'width' : bodyWidth
				}).parent().css('position', 'relative');
				if(thisBg.data('bgColor')){
					bgColor = thisBg.data('bgColor');
					thisBg.css('background-color', bgColor);
				}
				if(thisBg.parent().hasClass('text-banner') || thisBg.find('.text-banner').length || thisBg.find('.fullwidth-text-banners').length){
					bgHeight = thisBg.children().innerHeight();
					thisBg.parent().css('height', bgHeight - 2);
				}
			}
			if(thisBg.parent().hasClass('parallax-banners-wrapper')) {
					jQuery('.parallax-banners-wrapper').each(function(){
						block = jQuery(this).find('.text-banner');
						var wrapper = jQuery(this);
						var fullHeight = 0;
						var imgCount = block.size();
						var currentIndex = 0;
						block.each(function(){
							imgUrl = jQuery(this).css('background-image').replace(/url\(|\)|\"/ig, '');
							if(imgUrl.indexOf('none')==-1){
								img = new Image;
								img.src = imgUrl;
								img.setAttribute("name", jQuery(this).attr('id'));
								img.onload = function(){
									imgName = '#' + jQuery(this).attr('name');
									if(wrapper.data('fullscreen')){
										windowHeight = document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
										jQuery(imgName).css({
											'height' : windowHeight+'px',
											'background-size' : '100% 100%'
										});
										fullHeight += windowHeight;
									} else {
										jQuery(imgName).css('height', this.height+'px');
										jQuery(imgName).css('height', (this.height - 200)+'px');
										fullHeight += this.height - 200;
										// if (pixelRatio > 1) {
											// jQuery(imgName).css('background-size', this.width+'px' + ' ' + this.height+'px');
										// }
									}
									wrapper.css('height', fullHeight);
									currentIndex++;
									if(!jQuery('body').hasClass('mobile-device')){
										if(currentIndex == imgCount){
											if(jQuery(document.body).width() > 1278) {
												jQuery('#parallax-banner-1').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-2').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-3').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-4').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-5').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-6').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-7').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-8').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-9').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-10').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-11').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-12').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-13').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-14').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-15').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-16').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-17').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-18').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-19').parallax("60%", 0.7, false);
												jQuery('#parallax-banner-20').parallax("60%", 0.7, false);
											} else if(jQuery(document.body).width() > 977) {
												jQuery('#parallax-banner-1').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-2').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-3').parallax("60%", 0.9, false);
												jQuery('#parallax-banner-4').parallax("60%", 0.85, false);
												jQuery('#parallax-banner-5').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-6').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-7').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-8').parallax("60%", 0.9, false);
												jQuery('#parallax-banner-9').parallax("60%", 0.85, false);
												jQuery('#parallax-banner-10').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-11').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-12').parallax("60%", 0.9, false);
												jQuery('#parallax-banner-13').parallax("60%", 0.85, false);
												jQuery('#parallax-banner-14').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-15').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-16').parallax("60%", 0.9, false);
												jQuery('#parallax-banner-17').parallax("60%", 0.85, false);
												jQuery('#parallax-banner-18').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-19').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-20').parallax("60%", 0.9, false);
											} else if(jQuery(document.body).width() > 767) {
												jQuery('#parallax-banner-1').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-2').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-3').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-4').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-5').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-6').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-7').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-8').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-9').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-10').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-11').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-12').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-13').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-14').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-15').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-16').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-17').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-18').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-19').parallax("60%", 0.8, false);
												jQuery('#parallax-banner-20').parallax("60%", 0.8, false);
											} else {
												jQuery('#parallax-banner-1').parallax("30%", 0.5, true);
												jQuery('#parallax-banner-2').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-3').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-4').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-5').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-6').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-7').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-8').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-9').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-10').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-11').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-12').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-13').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-14').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-15').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-16').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-17').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-18').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-19').parallax("60%", 0.1, false);
												jQuery('#parallax-banner-20').parallax("60%", 0.1, false);
											}
										}
									}
								}
							}
							bannerText = jQuery(this).find('.banner-content');
							if(bannerText.data('top')){
								bannerText.css('top', bannerText.data('top'));
							}
							if(bannerText.data('left')){
								if(!bannerText.data('right')){
									bannerText.css({
										'left': bannerText.data('left'),
										'right' : 'auto'
									});
								} else {
									bannerText.css('left', bannerText.data('left'));
								}
							}
							if(bannerText.data('right')){
								if(!bannerText.data('left')){
									bannerText.css({
										'right': bannerText.data('right'),
										'left' : 'auto'
									});
								} else {
									bannerText.css('right', bannerText.data('right'));
								}
							}
						});
					});
					jQuery(window).scroll(function() {
						jQuery('.parallax-banners-wrapper').each(function(){
							block = jQuery(this).find('.text-banner');
							block.each(function(){
								var imagePos = jQuery(this).offset().top;
								var topOfWindow = jQuery(window).scrollTop();
								if (imagePos < topOfWindow+600) {
									jQuery(this).addClass("slideup");
								} else {
									jQuery(this).removeClass("slideup");
								}
							});
						});
					});
					setTimeout(function(){
						jQuery('#parallax-loading').fadeOut(200);
					}, 1000);
				}
				thisBg.animate({'opacity': 1}, 200)
		});
	}
}
function hoverImage(){
	jQuery('.product-image-photo').each(function(){
		if(jQuery(this).data('hoverImage')){
			hover_img = jQuery('<img />');
			hover_img.addClass('hover-image');
			hover_img.attr('src', jQuery(this).data('hoverImage'));
			hover_img.appendTo(jQuery(this).parent());
			jQuery(this).parents('.image-wrapper').addClass('hover-image');
		}
	});
}

var bsModal;
require(['jquery'], function ($)
{
    //require(['MeigeeBootstrap', 'MeigeeCarousel'], function(mb,mc)
    require(['MeigeeBootstrap'], function(mb,mc)
    {
    	bsModal = $.fn.modal.noConflict();
    	jQuery(document).ready(function(){
    		//customHomeSlider();
    		custom_top_menu_button();
    		replacingClass();
    		headerSearch();
    		headerCustomer();
    		custom_top_menu_button_listener();
    		/* Mobile Devices */
    		if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))){
    		/* Mobile Devices Class */
    			jQuery('body').addClass('mobile-device');
				var mobileDevice = true;
			}else if(!navigator.userAgent.match(/Android/i)){
				var mobileDevice = false;
			}

			/* Responsive */
			var responsiveflag = false;
			var topSelectFlag = false;
			var menu_type = jQuery('#nav').attr('class');




			jQuery('#sticky-header .search-button').on('click', function(){
				jQuery(this).toggleClass('active');
				jQuery('#sticky-header .block-search form.minisearch').slideToggle();
			});



			var isApple = false;
		/* apple position fixed fix */
		if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))){
			isApple = true;
			function stickyPosition(clear){
				items = jQuery('.header, .backstretch');
				if(clear == false){
					topIndent = jQuery(window).scrollTop();
					items.css({
						'position': 'absolute',
						'top': topIndent
					});
				}else{
					items.css({
						'position': 'fixed',
						'top': '0'
					});
				}
			}
			jQuery('#sticky-header .form-search input').on('focusin focusout', function(){
				jQuery(this).toggleClass('focus');
				if(jQuery('header.header').hasClass('floating')){
					if(jQuery(this).hasClass('focus')){
						setTimeout(function(){
							stickyPosition(false);
						}, 500);
					}else{
						stickyPosition(true);
					}
				}
			});
		}

			/* sticky header */
			if(jQuery('#sticky-header').length){
				var headerHeight = jQuery('.page-header').height();
				sticky = jQuery('#sticky-header');
				jQuery(window).on('scroll', function(){
					if(jQuery(document.body).width() > 977){
						if(!isApple){
							heightParam = headerHeight;
						}else{
							heightParam = headerHeight*2;
						}
						if(jQuery(this).scrollTop() >= heightParam){
							sticky.stop().slideDown(250);
							//jQuery(window).resize();
						}
						if(jQuery(this).scrollTop() < headerHeight ){
							sticky.stop().hide();
						}						

					}
				});
			}
			pageNotFound();
			accordionNav();
			jQuery(window).load(function(){
				accordionNav();
				backgroundWrapper();
    			custom_top_menu_button_listener();
			});
			hoverImage();
			jQuery(window).resize(function(){
				pageNotFound();
				accordionNav();
				replacingClass();
				backgroundWrapper();
				custom_top_menu_button_listener();
			});

			if(document.URL.indexOf("#product_tabs_reviews") != -1) {
				$('#tabs a[href="#product_tabs_reviews"]').tab('show')
			}
			$.fn.scrollTo = function (speed) {
				if (typeof(speed) === 'undefined')
					speed = 1000;
				$('html, body').animate({
					// scrollTop: parseInt($(this).offset().top)
					scrollTop: parseInt($('#tabs').offset().top - 100)
				}, speed);
			};
			$('.add-review').on('click', function(){
				$(this).scrollTo('#tabs');
				$('#tabs a[href="#product_tabs_reviews"]').tab('show');
			});

		});
	});

    require(['jquery/ui', 'MeigeeBootstrap', 'lightBox'], function(ui, lb)
    {
        $('*[data-toggle="lightbox"]').off().on('click', function(event)
        {
            event.preventDefault();
            $(this).ekkoLightbox();
            return false;
        });
    });


});










