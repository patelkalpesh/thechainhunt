cd /home/thechain/public_html
#php bin/magento setup:backup --db
d=$(date +%Y-%m-%d_%H-%M)
tar cvfz /home/thechain/$d-backup.tgz /home/thechain/public_html/* --exclude='var/log' --exclude='var/session' --exclude='var/cache' --exclude='var/backup' --exclude='var/report' --exclude='pub/media/catalog/product/cache' --exclude='staging' --exclude='var/page_cache' --exclude='var/composer_home' --exclude='var/generation' --exclude='var/di' --exclude='var/view_preprocessed'

